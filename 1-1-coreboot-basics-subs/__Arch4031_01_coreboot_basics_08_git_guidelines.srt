﻿1
00:00:00,560 --> 00:00:04,960
So, let's start with a little bit of git

2
00:00:03,760 --> 00:00:08,240
guidelines

3
00:00:04,960 --> 00:00:09,519
and maybe let's begin with cloning

4
00:00:08,240 --> 00:00:13,280
repository

5
00:00:09,519 --> 00:00:16,400
and configuring git. This will provide

6
00:00:13,280 --> 00:00:19,680
some basic capabilities

7
00:00:16,400 --> 00:00:22,240
for further committing and

8
00:00:19,680 --> 00:00:24,400
pushing patches to repository, we will

9
00:00:22,240 --> 00:00:27,279
not develop code in this training

10
00:00:24,400 --> 00:00:29,679
but this is very useful if you will try

11
00:00:27,279 --> 00:00:32,239
to fix something to improve something.

12
00:00:29,679 --> 00:00:33,840
We encourage you to contribute to the

13
00:00:32,239 --> 00:00:36,719
community

14
00:00:33,840 --> 00:00:39,200
and it is important to make everything

15
00:00:36,719 --> 00:00:41,840
set up correctly.

16
00:00:39,200 --> 00:00:43,600
Every patch that you sign to community

17
00:00:41,840 --> 00:00:44,399
have to be signed off with your real

18
00:00:43,600 --> 00:00:48,079
name,

19
00:00:44,399 --> 00:00:49,920
which means that you claim that you

20
00:00:48,079 --> 00:00:50,719
agree with developer certificate of

21
00:00:49,920 --> 00:00:54,079
origin,

22
00:00:50,719 --> 00:00:58,800
which you can find on internet easily.

23
00:00:54,079 --> 00:01:02,399
Yes, so let's go with

24
00:00:58,800 --> 00:01:02,399
some demo of these commands.

