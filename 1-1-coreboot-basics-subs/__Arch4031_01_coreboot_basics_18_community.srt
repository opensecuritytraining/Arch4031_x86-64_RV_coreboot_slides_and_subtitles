﻿1
00:00:00,074 --> 00:00:01,023
Finally,

2
00:00:01,023 --> 00:00:03,061
some references to community resources.

3
00:00:03,062 --> 00:00:04,003
Of course,

4
00:00:04,003 --> 00:00:05,083
I mentioned documentation

5
00:00:05,084 --> 00:00:06,005


6
00:00:06,051 --> 00:00:06,097


7
00:00:06,097 --> 00:00:07,088
doc.coreboot.org

8
00:00:07,089 --> 00:00:08,076
There is Wiki,

9
00:00:08,076 --> 00:00:10,027
which is right now deprecated,

10
00:00:10,027 --> 00:00:13,006
but still has a lot of valuable information.

11
00:00:13,007 --> 00:00:13,061
There is

12
00:00:13,061 --> 00:00:14,027
blog

13
00:00:14,028 --> 00:00:15,042


14
00:00:15,043 --> 00:00:16,058
where you from

15
00:00:16,058 --> 00:00:18,003
time to time and get some news,

16
00:00:18,004 --> 00:00:21,005
get some information from coreboot project.

17
00:00:21,044 --> 00:00:25,042
There are IRC channels at freenode,

18
00:00:25,043 --> 00:00:26,015


19
00:00:26,016 --> 00:00:26,005


20
00:00:26,051 --> 00:00:27,088
there's coreboot channel,

21
00:00:27,088 --> 00:00:29,037
there is flashrom channel,

22
00:00:29,038 --> 00:00:32,032
there is also Slack, u-root

23
00:00:32,033 --> 00:00:33,056
slack.com

24
00:00:34,024 --> 00:00:35,058
There's mailing list

25
00:00:35,059 --> 00:00:36,046


26
00:00:36,047 --> 00:00:39,052
there is the Gerrit where you can review the code.

27
00:00:39,053 --> 00:00:44,014
There is official coreboot source called mirror on GitHub

28
00:00:44,015 --> 00:00:45,085
Facebook account,

29
00:00:45,085 --> 00:00:47,003
Twitter account.

30
00:00:47,004 --> 00:00:49,081


31
00:00:49,082 --> 00:00:50,003
So,

32
00:00:50,003 --> 00:00:50,053
typically,

33
00:00:50,053 --> 00:00:54,046
those locations are occupied in

34
00:00:54,064 --> 00:00:56,017


35
00:00:56,018 --> 00:00:57,087
Central Europe Time zone.

36
00:00:57,088 --> 00:00:58,083


37
00:00:58,084 --> 00:01:00,019
Sometimes,

38
00:01:00,019 --> 00:01:02,003
like probably also US

39
00:01:02,004 --> 00:01:04,004


40
00:01:04,041 --> 00:01:07,005
East coast time.

41
00:01:07,023 --> 00:01:10,005
Since there are many developers from those locations.

