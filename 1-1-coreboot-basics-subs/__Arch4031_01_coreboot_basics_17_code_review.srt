﻿1
00:00:00,084 --> 00:00:01,049
Okay,

2
00:00:01,049 --> 00:00:02,085
let's talk about code review.

3
00:00:04,034 --> 00:00:07,046
So, during code review your changes are graded

4
00:00:08,024 --> 00:00:09,055
by

5
00:00:09,055 --> 00:00:10,036
reviewers,

6
00:00:10,074 --> 00:00:11,011


7
00:00:11,012 --> 00:00:15,086
-2 is reserved at grade for code developers.

8
00:00:15,087 --> 00:00:17,009
It means, definitely don't merge.

9
00:00:17,091 --> 00:00:20,062
So, it means that if there is some serious problem

10
00:00:20,062 --> 00:00:23,042
with the patch set,

11
00:00:23,043 --> 00:00:26,078
either things are very low quality,

12
00:00:26,078 --> 00:00:29,084
either are made in wrong way, either

13
00:00:29,085 --> 00:00:32,051
maybe they are incorrect

14
00:00:32,052 --> 00:00:37,016
and not according to coreboot project vision.

15
00:00:37,017 --> 00:00:37,098


16
00:00:37,099 --> 00:00:43,037
So, those are like serious problems which probably require a

17
00:00:43,037 --> 00:00:48,045
lot of work to be improved or unblocking something on

18
00:00:48,045 --> 00:00:50,003
the vision

19
00:00:50,003 --> 00:00:54,017
strategy level. -1 means, I prefer you want to

20
00:00:54,017 --> 00:00:54,082
merge it,

21
00:00:54,083 --> 00:00:55,046
that means

22
00:00:55,046 --> 00:00:58,014
like there are some improvements needed

23
00:00:58,015 --> 00:01:00,026


24
00:01:00,027 --> 00:01:01,017
this is like,

25
00:01:01,018 --> 00:01:04,086
I just don't want this to be merged because this

26
00:01:04,086 --> 00:01:05,065
is low quality.

27
00:01:06,004 --> 00:01:07,004
0 is neutral,

28
00:01:07,004 --> 00:01:09,021
+1 means looks good,

29
00:01:09,022 --> 00:01:11,097
but someone else should check it.

30
00:01:11,098 --> 00:01:12,095
For example,

31
00:01:12,095 --> 00:01:17,059
we use +1 if we review changes of our

32
00:01:17,006 --> 00:01:18,081
company members.

33
00:01:18,082 --> 00:01:22,077
So, we never give +2 for departures

34
00:01:22,078 --> 00:01:24,046
that coming from three in depth.

35
00:01:25,004 --> 00:01:25,077


36
00:01:25,078 --> 00:01:28,008
+2 means please merge it,

37
00:01:28,081 --> 00:01:34,031
and when the maintainers of coreboot will go through the

38
00:01:34,032 --> 00:01:35,073
changes and we'll see,

39
00:01:35,073 --> 00:01:36,057
+2,

40
00:01:36,058 --> 00:01:36,085


41
00:01:36,086 --> 00:01:40,056
this will be would be merged. There are some rules

42
00:01:40,056 --> 00:01:42,076
related to +2 grade,

43
00:01:43,024 --> 00:01:48,002
the plus two should be never gave to your

44
00:01:48,003 --> 00:01:49,028
own change,

45
00:01:49,029 --> 00:01:50,077
if you give +2,

46
00:01:50,078 --> 00:01:52,081
then you are responsible for those changes.

47
00:01:52,081 --> 00:01:53,072
So,

48
00:01:53,073 --> 00:01:56,007
if this change will break something,

49
00:01:56,008 --> 00:01:58,083
then you are expected to be involved in fixing the

50
00:01:58,083 --> 00:01:59,035
things.

51
00:02:00,064 --> 00:02:01,045


52
00:02:02,034 --> 00:02:03,011
Of course,

53
00:02:03,012 --> 00:02:04,005


54
00:02:04,044 --> 00:02:05,024


55
00:02:05,094 --> 00:02:06,045


56
00:02:06,045 --> 00:02:08,062
you should wait for the review.

57
00:02:08,062 --> 00:02:11,058
It's not like you will get +2 in

58
00:02:11,058 --> 00:02:13,003
24 hours.

59
00:02:13,004 --> 00:02:14,035
Typically,

60
00:02:14,036 --> 00:02:17,094
developers from all around the world should see the change.

61
00:02:17,095 --> 00:02:18,093
If it is simple,

62
00:02:18,093 --> 00:02:21,071
it's probably easier if it is long and complex

63
00:02:21,071 --> 00:02:23,006
it may take a lot,

64
00:02:23,044 --> 00:02:23,091


65
00:02:23,092 --> 00:02:28,041
and the waiting period and time that you wait to

66
00:02:28,042 --> 00:02:32,034
ping to ask when I can get answer should be

67
00:02:32,034 --> 00:02:34,019
longer for bigger changes.

68
00:02:34,002 --> 00:02:37,036
This is similar to any open source project,

69
00:02:37,094 --> 00:02:43,026
and you should not expect anyone to review your changes

70
00:02:43,035 --> 00:02:44,016


71
00:02:44,084 --> 00:02:47,005
you should ask for this review.

72
00:02:47,006 --> 00:02:50,001
There is a script

73
00:02:50,001 --> 00:02:51,023
get maintainers,

74
00:02:51,023 --> 00:02:53,006
you can easily also check who

75
00:02:53,044 --> 00:02:54,045


76
00:02:54,046 --> 00:02:56,068
modified the same region of code

77
00:02:56,069 --> 00:02:57,058


78
00:02:57,059 --> 00:03:01,045
in the past months, there is MAINTAINERS file

79
00:03:01,046 --> 00:03:06,004
you can also check who's responsible for given section, for

80
00:03:06,005 --> 00:03:13,063
given SOC, for given module, for given platform. That's the basic

81
00:03:13,063 --> 00:03:14,076
way to verify.

82
00:03:15,024 --> 00:03:15,073


83
00:03:15,074 --> 00:03:18,011
If you look for reviewers,

84
00:03:18,011 --> 00:03:20,094
you can always ask on IRC on Slack

85
00:03:20,095 --> 00:03:21,045


86
00:03:21,046 --> 00:03:23,007
and then if you for a long time not getting

87
00:03:23,007 --> 00:03:24,036
any response,

88
00:03:24,036 --> 00:03:26,085
you can go to my link list and ask,

89
00:03:27,024 --> 00:03:28,045
"Hey,

90
00:03:28,046 --> 00:03:30,058
can you take a look at these changes ?

91
00:03:30,059 --> 00:03:32,072
it's here for a long time..."

92
00:03:32,073 --> 00:03:33,045


93
00:03:33,093 --> 00:03:37,031
And explain why it would be great to merge

94
00:03:37,031 --> 00:03:37,086
this code.

