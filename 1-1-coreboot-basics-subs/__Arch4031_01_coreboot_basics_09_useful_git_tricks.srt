﻿1
00:00:00,054 --> 00:00:04,009
Working with open source projects like Linux kernel or coreboot

2
00:00:04,009 --> 00:00:11,052
project requires my studying git and preferably git command line

3
00:00:11,053 --> 00:00:12,046
interface.

4
00:00:13,024 --> 00:00:13,062


5
00:00:13,063 --> 00:00:16,051
So, why git command line?

6
00:00:16,052 --> 00:00:19,002
This is because the user interface,

7
00:00:19,021 --> 00:00:21,062
like all the wrappers around,

8
00:00:21,062 --> 00:00:21,096
git,

9
00:00:22,034 --> 00:00:29,015
typically cause a lot of problems. They hiding something

10
00:00:29,015 --> 00:00:30,017
They kind of

11
00:00:30,018 --> 00:00:30,087


12
00:00:30,088 --> 00:00:31,068
cause,

13
00:00:31,069 --> 00:00:32,032


14
00:00:32,033 --> 00:00:37,048
situation in which we don't precisely understand what's happening behind

15
00:00:37,048 --> 00:00:38,005
the scene.

16
00:00:38,044 --> 00:00:42,053
Especially, when someone during the review asked us to replace

17
00:00:42,053 --> 00:00:44,067
patches or change the order.

18
00:00:44,068 --> 00:00:49,084
It requires mastering the command line git interface. To

19
00:00:49,084 --> 00:00:54,061
simplify that I'm using for more than 10 years,

20
00:00:54,062 --> 00:00:55,012


21
00:00:55,013 --> 00:00:58,036
following a set of aliases.

22
00:00:58,084 --> 00:01:00,055
So, alias is for

23
00:01:00,094 --> 00:01:01,042


24
00:01:01,043 --> 00:01:05,085
commit and signing, for checking out, for branching,

25
00:01:05,086 --> 00:01:09,064
for checking status, for checking the difference

26
00:01:09,065 --> 00:01:10,047


27
00:01:10,048 --> 00:01:14,004
for checking the differences which are already in staging,

28
00:01:14,005 --> 00:01:15,004


29
00:01:15,041 --> 00:01:20,044
also like there is very useful stuff around log command,

30
00:01:20,045 --> 00:01:26,027
which gives you the ability to draw cool graphs in

31
00:01:26,028 --> 00:01:27,026
console.

32
00:01:28,004 --> 00:01:30,048
So, let me present

33
00:01:30,049 --> 00:01:31,016


34
00:01:31,054 --> 00:01:33,055
those aliases to you.

