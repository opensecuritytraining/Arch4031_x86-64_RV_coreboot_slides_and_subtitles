﻿1
00:00:00,064 --> 00:00:01,096
Before you push,

2
00:00:01,097 --> 00:00:06,043
you should rebase your code to the master to the

3
00:00:06,043 --> 00:00:09,052
upstream master branch and re-tests

4
00:00:09,053 --> 00:00:12,046
if nothing changed in the behavior of your feature,

5
00:00:13,004 --> 00:00:14,004
to be honest,

6
00:00:14,004 --> 00:00:14,057


7
00:00:14,058 --> 00:00:16,081
it's good to test even minor changes,

8
00:00:16,082 --> 00:00:19,036
it happened a lot during development that,

9
00:00:19,074 --> 00:00:20,048


10
00:00:20,049 --> 00:00:27,063
you did some minor change and the behavior change in unexpected

11
00:00:27,063 --> 00:00:28,021
way.

12
00:00:28,022 --> 00:00:33,006
So, it's good to test your changes before pushing upstream.

13
00:00:33,064 --> 00:00:37,051
Make sure that every source file has proper license notice

14
00:00:37,051 --> 00:00:38,057


15
00:00:38,058 --> 00:00:41,084
make sure that all changes have been committed.

16
00:00:41,085 --> 00:00:45,017
Because, maybe someone will try to reproduce that

17
00:00:45,018 --> 00:00:46,059
or simply

18
00:00:46,006 --> 00:00:47,016


19
00:00:47,017 --> 00:00:49,013
if not all files are committed

20
00:00:49,014 --> 00:00:53,046
Jenkins will quickly fail with building your changes and your

21
00:00:53,046 --> 00:00:54,066
patches will be rejected.

22
00:00:55,014 --> 00:00:55,086


23
00:00:56,054 --> 00:01:00,002
Other thing is that Gerrit requires you to

24
00:01:00,002 --> 00:01:05,036
push to HEAD:refs/for/master instead of master

25
00:01:05,037 --> 00:01:05,084


26
00:01:05,085 --> 00:01:06,082
this is important,

27
00:01:06,082 --> 00:01:09,035
because if you will try to push the master,

28
00:01:09,036 --> 00:01:11,005
then you will quickly get some,

29
00:01:11,006 --> 00:01:12,097
error.

30
00:01:12,098 --> 00:01:13,069


31
00:01:13,007 --> 00:01:15,043
You should check

32
00:01:15,044 --> 00:01:15,094


33
00:01:15,095 --> 00:01:19,046
changes against some basic tests.

34
00:01:19,074 --> 00:01:21,019
The first one,

35
00:01:21,019 --> 00:01:23,007
what-Jenkins-does is quite long.

36
00:01:23,071 --> 00:01:27,006
So, maybe not every time you should run it,

37
00:01:27,044 --> 00:01:30,065
maybe once or maybe it's not in it at all.

38
00:01:30,066 --> 00:01:32,067
The lint / lint-stable,

39
00:01:32,067 --> 00:01:34,039
those are already running,

40
00:01:34,004 --> 00:01:36,046
thanks to your git hooks,

41
00:01:36,047 --> 00:01:38,015
test-basic,

42
00:01:38,016 --> 00:01:38,077


43
00:01:38,078 --> 00:01:39,004


44
00:01:39,005 --> 00:01:41,037
we'll run some standard build test.

45
00:01:41,038 --> 00:01:41,098


46
00:01:41,099 --> 00:01:44,015
and all

47
00:01:44,016 --> 00:01:44,053
of course,

48
00:01:44,053 --> 00:01:45,038
should pass.

49
00:01:45,039 --> 00:01:46,084
And there is a test line

50
00:01:46,085 --> 00:01:47,066


51
00:01:47,094 --> 00:01:52,092
this run some stable and extended lean tests.

52
00:01:52,093 --> 00:01:55,008
And to push your code

53
00:01:55,008 --> 00:01:57,011
you just type git push origin HEAD:refs/for/master

54
00:01:57,011 --> 00:01:58,019


55
00:01:58,002 --> 00:02:00,055


56
00:02:00,055 --> 00:02:06,086
And this should create Gerrit merge

57
00:02:06,086 --> 00:02:08,027
request or Gerrit

58
00:02:08,041 --> 00:02:09,085


59
00:02:10,034 --> 00:02:14,027
ticket. In which you can track your review,

60
00:02:14,028 --> 00:02:16,077
you can discuss your changes with community,

61
00:02:16,078 --> 00:02:19,025
and then

62
00:02:19,026 --> 00:02:23,094
the community can kind of give you feedback about

63
00:02:23,095 --> 00:02:24,008


64
00:02:24,081 --> 00:02:25,046
what you provide.

