﻿1
00:00:00,399 --> 00:00:04,160
So, let's try to answer some very very

2
00:00:03,199 --> 00:00:07,359
basic question.

3
00:00:04,160 --> 00:00:08,480
What is coreboot? So, coreboot by

4
00:00:07,359 --> 00:00:10,639
website definition,

5
00:00:08,480 --> 00:00:11,599
is a extended firmware platform that

6
00:00:10,639 --> 00:00:13,360
delivers

7
00:00:11,599 --> 00:00:15,519
lightning fast and secure boot

8
00:00:13,360 --> 00:00:16,880
experience on modern computers and

9
00:00:15,519 --> 00:00:19,680
embedded systems.

10
00:00:16,880 --> 00:00:20,720
It's an open source project and it

11
00:00:19,680 --> 00:00:23,680
provides

12
00:00:20,720 --> 00:00:24,880
audibility and control over the

13
00:00:23,680 --> 00:00:29,039
technology.

14
00:00:24,880 --> 00:00:32,079
So, in general it is not

15
00:00:29,039 --> 00:00:34,800
a firmware per se it's more like a

16
00:00:32,079 --> 00:00:36,480
firmware framework or extensible

17
00:00:34,800 --> 00:00:40,320
firmware platform

18
00:00:36,480 --> 00:00:43,440
which can give you some

19
00:00:40,320 --> 00:00:46,399
features like very fast boots some

20
00:00:43,440 --> 00:00:47,039
audibility because it is open source

21
00:00:46,399 --> 00:00:50,480
it has

22
00:00:47,039 --> 00:00:51,280
it has many secure features

23
00:00:50,480 --> 00:00:54,239
which were

24
00:00:51,280 --> 00:00:56,879
liked by many vendors which I will

25
00:00:54,239 --> 00:01:00,480
explain on the next slides.

26
00:00:56,879 --> 00:01:01,440
So, coreboot was created in 1999 by Ron

27
00:01:00,480 --> 00:01:03,840
Minnich,

28
00:01:01,440 --> 00:01:05,840
in the same year by the way u-boot was

29
00:01:03,840 --> 00:01:10,240
created so it's interesting

30
00:01:05,840 --> 00:01:12,720
coincident. Motivation of Ron

31
00:01:10,240 --> 00:01:13,520
with whose picture we have here on the

32
00:01:12,720 --> 00:01:16,799
slide

33
00:01:13,520 --> 00:01:17,759
was, why I have to press F1 on all

34
00:01:16,799 --> 00:01:20,880
these PCs

35
00:01:17,759 --> 00:01:24,000
in cluster that I'm trying to build when

36
00:01:20,880 --> 00:01:26,240
Ron worked in Los Alamos Laboratory.

37
00:01:24,000 --> 00:01:28,320
He just have to run with the keyboard

38
00:01:26,240 --> 00:01:31,520
and push F1 to

39
00:01:28,320 --> 00:01:34,560
boot the cluster. So, he decided he will

40
00:01:31,520 --> 00:01:37,920
just hack it and get rid of this F1

41
00:01:34,560 --> 00:01:40,479
but the project quickly grow up and

42
00:01:37,920 --> 00:01:42,159
 it's he started to create

43
00:01:40,479 --> 00:01:44,880
something called LinuxBIOS and

44
00:01:42,159 --> 00:01:45,280
which was then converted to coreboot.

45
00:01:44,880 --> 00:01:48,320
This

46
00:01:45,280 --> 00:01:49,040
key story I will explain to you. Corboot

47
00:01:48,320 --> 00:01:52,159
supports

48
00:01:49,040 --> 00:01:56,000
many architectures

49
00:01:52,159 --> 00:01:58,079
ARM, ARM64 up to version 4.11

50
00:01:56,000 --> 00:01:59,840
there was support for mips but

51
00:01:58,079 --> 00:02:01,920
unfortunately there are no

52
00:01:59,840 --> 00:02:04,159
maintainers for mips that's why

53
00:02:01,920 --> 00:02:05,200
the platform was removed. There is power

54
00:02:04,159 --> 00:02:08,959
PC support

55
00:02:05,200 --> 00:02:12,080
which is very like small risk five

56
00:02:08,959 --> 00:02:14,000
and coreboot was one of the first open

57
00:02:12,080 --> 00:02:17,040
source firmware projects to support

58
00:02:14,000 --> 00:02:19,840
risk five and of course x86.

59
00:02:17,040 --> 00:02:22,480
Recently 3M Dev started work on

60
00:02:19,840 --> 00:02:24,879
supporting power nine which

61
00:02:22,480 --> 00:02:27,040
will add even more architectures,

62
00:02:24,879 --> 00:02:29,440
more modern architectures to

63
00:02:27,040 --> 00:02:31,200
the bucket. Please note that

64
00:02:29,440 --> 00:02:32,959
coreboot is always written

65
00:02:31,200 --> 00:02:34,840
with small letters starting with small

66
00:02:32,959 --> 00:02:36,959
letter even at the beginning of the

67
00:02:34,840 --> 00:02:40,239
sentence.

68
00:02:36,959 --> 00:02:42,319
So, the idea behind coreboot is

69
00:02:40,239 --> 00:02:45,440
single responsibility principle

70
00:02:42,319 --> 00:02:46,239
so it tries to do one small thing just

71
00:02:45,440 --> 00:02:48,400
the hardware

72
00:02:46,239 --> 00:02:49,280
initialization for a given hardware

73
00:02:48,400 --> 00:02:52,160
platform,

74
00:02:49,280 --> 00:02:52,800
but of course this is

75
00:02:52,160 --> 00:02:56,080
complex

76
00:02:52,800 --> 00:02:58,560
and this is hard to keep but

77
00:02:56,080 --> 00:03:00,959
after this initialization is done, coreboot

78
00:02:58,560 --> 00:03:04,080
assumes it's over

79
00:03:00,959 --> 00:03:05,360
and it can move responsibility

80
00:03:04,080 --> 00:03:08,560
to someone else which

81
00:03:05,360 --> 00:03:10,159
typically should be in payload

82
00:03:08,560 --> 00:03:13,040
and of course, we'll explain what that

83
00:03:10,159 --> 00:03:15,519
means in further slides.

84
00:03:13,040 --> 00:03:17,040
So, many people ask what's

85
00:03:15,519 --> 00:03:19,280
the difference with UEFI

86
00:03:17,040 --> 00:03:21,040
so you first of all UEFI is just the

87
00:03:19,280 --> 00:03:24,640
specification which I will explain

88
00:03:21,040 --> 00:03:27,040
but many people call UEFI

89
00:03:24,640 --> 00:03:28,319
some implementation or some BIOS which

90
00:03:27,040 --> 00:03:32,080
is already on some

91
00:03:28,319 --> 00:03:36,319
platform or they mean EDK2

92
00:03:32,080 --> 00:03:40,000
reference implementation. So, first of all

93
00:03:36,319 --> 00:03:43,360
coreboot builds ready to boot

94
00:03:40,000 --> 00:03:46,400
image, ready to boot BIOS

95
00:03:43,360 --> 00:03:48,879
which you can flash into your SPI and

96
00:03:46,400 --> 00:03:51,120
boot the platform, this is not the case

97
00:03:48,879 --> 00:03:52,480
of UEFI, UEFI it's just a framework even

98
00:03:51,120 --> 00:03:54,720
EDK2

99
00:03:52,480 --> 00:03:55,840
miss many components to build

100
00:03:54,720 --> 00:03:58,159
the platform.

101
00:03:55,840 --> 00:03:59,760
Second thing, coreboot right now

102
00:03:58,159 --> 00:04:03,120
supports around

103
00:03:59,760 --> 00:04:03,920
300 boards from more than 70

104
00:04:03,120 --> 00:04:06,959
vendors.

105
00:04:03,920 --> 00:04:08,560
So, we're just building flashing and

106
00:04:06,959 --> 00:04:10,239
and booting coreboot

107
00:04:08,560 --> 00:04:12,480
on those platforms and hopefully, we will

108
00:04:10,239 --> 00:04:15,120
see more and more platforms there.

109
00:04:12,480 --> 00:04:16,639
coreboot has different target than

110
00:04:15,120 --> 00:04:19,840
for example u-boot

111
00:04:16,639 --> 00:04:22,720
it is interested more in server

112
00:04:19,840 --> 00:04:24,240
that desktops and of course sometimes

113
00:04:22,720 --> 00:04:26,720
embedded systems but

114
00:04:24,240 --> 00:04:28,320
this is not the main goal, I

115
00:04:26,720 --> 00:04:30,960
believe of course like

116
00:04:28,320 --> 00:04:31,360
of course embedded is supported. But, you

117
00:04:30,960 --> 00:04:34,479
boot

118
00:04:31,360 --> 00:04:36,639
is more in small ARM

119
00:04:34,479 --> 00:04:38,479
environments more popular more major

120
00:04:36,639 --> 00:04:40,240
there,

121
00:04:38,479 --> 00:04:43,199
and of course, we will explain

122
00:04:40,240 --> 00:04:46,560
differences between various

123
00:04:43,199 --> 00:04:49,360
open source firmware projects

124
00:04:46,560 --> 00:04:49,360
in this presentation.

