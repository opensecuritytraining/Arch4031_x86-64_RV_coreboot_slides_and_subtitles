﻿1
00:00:00,034 --> 00:00:02,012
In next practice exercise,

2
00:00:02,012 --> 00:00:02,078
we will

3
00:00:02,079 --> 00:00:04,003
first of all,

4
00:00:04,003 --> 00:00:06,037
check if Docker is configured.

5
00:00:06,038 --> 00:00:09,021
We will check what kind of images we already have

6
00:00:09,022 --> 00:00:10,032
downloaded,

7
00:00:10,033 --> 00:00:13,001
we should have at least have the debian if we had

8
00:00:13,002 --> 00:00:14,095
cleared Docker installation.

9
00:00:15,064 --> 00:00:20,016
We may pull coreboot sdk,

10
00:00:20,017 --> 00:00:24,064
which is official coreboot Docker container with the toolchain.

11
00:00:24,064 --> 00:00:25,046


12
00:00:25,047 --> 00:00:26,088
There are some problems with it,

13
00:00:26,089 --> 00:00:29,046
I will describe that in the next slide.

14
00:00:29,094 --> 00:00:33,045
But for this training, we will use 3mdeb

15
00:00:33,045 --> 00:00:34,089
coreboot-training-sdk,

16
00:00:34,009 --> 00:00:40,086
which is already published in version 4.12.1 on the Docker

17
00:00:40,086 --> 00:00:41,025
hub.

18
00:00:41,064 --> 00:00:43,069
What's the versioning scheme?

19
00:00:43,007 --> 00:00:49,019
So, the problem is a coreboot sdk lost versioning

20
00:00:49,019 --> 00:00:50,069
scheme at some point,

21
00:00:50,007 --> 00:00:51,025


22
00:00:51,026 --> 00:00:54,053
this is probably some in our maintainability problem.

23
00:00:54,054 --> 00:00:55,078
But for us,

24
00:00:55,078 --> 00:00:57,049
we would like to keep,

25
00:00:57,005 --> 00:00:58,014


26
00:00:58,015 --> 00:01:03,099
the versioning scheme of our training sdk as containing

27
00:01:03,099 --> 00:01:06,021
3 numbers, a major version of coreboot,

28
00:01:06,021 --> 00:01:08,005
as a first number,

29
00:01:08,024 --> 00:01:12,002
second number minor version of coreboot and third number

30
00:01:12,002 --> 00:01:14,007
coreboot training sdk patch,

31
00:01:14,008 --> 00:01:18,035
which we will increase with the changes to the Docker

32
00:01:18,035 --> 00:01:19,006
container.

33
00:01:19,064 --> 00:01:22,036
All the versions will be available on,

34
00:01:22,074 --> 00:01:23,023


35
00:01:23,024 --> 00:01:24,041
Docker hub,

36
00:01:24,042 --> 00:01:27,053
link to the Docker hub you may find under this

37
00:01:27,054 --> 00:01:32,004
video, and link to official coreboot sdk also will be

38
00:01:32,004 --> 00:01:32,076
there.

