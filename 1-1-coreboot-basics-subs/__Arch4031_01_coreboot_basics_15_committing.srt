﻿1
00:00:00,094 --> 00:00:01,057
Committing,

2
00:00:01,057 --> 00:00:05,042
so if you are committing any coreboot repository

3
00:00:05,042 --> 00:00:09,099
you should follow Linux kernel submitting patches documentation

4
00:00:10,000 --> 00:00:14,005
the most important rules are that there should be only

5
00:00:14,005 --> 00:00:15,068
1 logical change per commit.

6
00:00:15,069 --> 00:00:18,055
It's very important because if you mix

7
00:00:18,064 --> 00:00:20,049
couple of fixes in 1 commit,

8
00:00:20,049 --> 00:00:23,084
then it's hard to separate that and hard to understand,

9
00:00:23,085 --> 00:00:25,048
if there will be any bug,

10
00:00:25,048 --> 00:00:26,004
introduced

11
00:00:26,004 --> 00:00:28,063
It's hard to understand what caused that bug,

12
00:00:28,063 --> 00:00:31,048
what does not cause that bug.

13
00:00:31,049 --> 00:00:32,016


14
00:00:32,064 --> 00:00:37,014
All comments should contain three sections separated by empty lines.

15
00:00:37,014 --> 00:00:41,009
So, first section is title, second section is

16
00:00:41,009 --> 00:00:45,066
description, and third section is signed-off region.

17
00:00:46,024 --> 00:00:50,015
Title typically should not be bigger than

18
00:00:50,016 --> 00:00:52,023
65 characters,

19
00:00:52,024 --> 00:00:55,028
typical method is that

20
00:00:55,029 --> 00:00:55,079
first,

21
00:00:55,079 --> 00:00:57,057
to play some module.

22
00:00:57,058 --> 00:01:01,099
This is used in various ways,

23
00:01:02,000 --> 00:01:05,099
the best way to comply with current rules is just

24
00:01:06,000 --> 00:01:07,044
check, "Okay,

25
00:01:07,044 --> 00:01:07,065
well,

26
00:01:07,066 --> 00:01:09,016
how the files

27
00:01:09,016 --> 00:01:12,021
which I modify or how the directories

28
00:01:12,022 --> 00:01:16,004
which are near the location that I do the modification

29
00:01:16,005 --> 00:01:18,068
were documented in commit messages."

30
00:01:18,068 --> 00:01:21,084
So, it's good to look at git log and see

31
00:01:21,084 --> 00:01:23,087
how those changes were committed,

32
00:01:23,087 --> 00:01:25,046
then you can follow the same pattern.

33
00:01:25,074 --> 00:01:29,008
But, typically it is like a component

34
00:01:29,009 --> 00:01:29,097


35
00:01:29,098 --> 00:01:31,042
and then we have

36
00:01:31,043 --> 00:01:31,092


37
00:01:31,093 --> 00:01:35,006
title associated with this component.

38
00:01:36,054 --> 00:01:37,061


39
00:01:37,062 --> 00:01:39,088
So, then we have this explanation, explanation

40
00:01:39,088 --> 00:01:40,056
should be

41
00:01:41,034 --> 00:01:42,015


42
00:01:42,016 --> 00:01:47,021
mostly why we do this change, what that change really

43
00:01:47,021 --> 00:01:48,008
does,

44
00:01:48,009 --> 00:01:50,048
what's the benefit of it?

45
00:01:50,049 --> 00:01:51,044


46
00:01:51,045 --> 00:01:52,039
And like,

47
00:01:52,004 --> 00:01:55,007
you have to do kind of marketing pitch,

48
00:01:55,071 --> 00:01:59,051
why this is needed and why this improves the

49
00:01:59,051 --> 00:02:00,008
situation.

50
00:02:00,008 --> 00:02:03,041
So, if some reviewer will read that

51
00:02:03,041 --> 00:02:06,068
he/she will be convinced that this has to be

52
00:02:06,068 --> 00:02:07,026
merged.

