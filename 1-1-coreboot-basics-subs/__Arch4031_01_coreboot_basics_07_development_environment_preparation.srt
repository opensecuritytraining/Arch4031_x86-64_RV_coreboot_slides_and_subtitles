﻿1
00:00:00,480 --> 00:00:05,600
Okay, so we get through history

2
00:00:03,040 --> 00:00:07,200
and let's go to development environment

3
00:00:05,600 --> 00:00:09,840
preparation.

4
00:00:07,200 --> 00:00:11,840
So, as a open source firmware

5
00:00:09,840 --> 00:00:15,280
enthusiast we definitely prefer

6
00:00:11,840 --> 00:00:19,279
Linux. All the procedures

7
00:00:15,280 --> 00:00:20,960
are probably possible on Windows but

8
00:00:19,279 --> 00:00:24,080
we never try that

9
00:00:20,960 --> 00:00:28,320
and if you want it's it's on your own.

10
00:00:24,080 --> 00:00:30,000
From my experience, any

11
00:00:28,320 --> 00:00:32,880
modern Linux distribution

12
00:00:30,000 --> 00:00:33,440
which support Docker, should work for you.

13
00:00:32,880 --> 00:00:35,200


14
00:00:33,440 --> 00:00:37,120
What is Docker? I hope you know what is

15
00:00:35,200 --> 00:00:41,280
Docker, which is very popular

16
00:00:37,120 --> 00:00:44,320
new containers technology.

17
00:00:41,280 --> 00:00:45,600
So, what containers are OS level

18
00:00:44,320 --> 00:00:48,079
virtualization,

19
00:00:45,600 --> 00:00:49,680
which is special feature of

20
00:00:48,079 --> 00:00:52,719
Linux kernel called C

21
00:00:49,680 --> 00:00:54,879
groups. It is used for

22
00:00:52,719 --> 00:00:56,800
isolating some resources. So, for example

23
00:00:54,879 --> 00:00:59,920
you can create kind of

24
00:00:56,800 --> 00:01:02,719
a fake operating system inside your

25
00:00:59,920 --> 00:01:06,400
operating system or multiple of those.

26
00:01:02,719 --> 00:01:08,479
What this gives,

27
00:01:06,400 --> 00:01:09,840
so it is important to understand that

28
00:01:08,479 --> 00:01:13,520
embedded software or

29
00:01:09,840 --> 00:01:15,600
firmware development requires

30
00:01:13,520 --> 00:01:17,439
a bit different building environment. It

31
00:01:15,600 --> 00:01:20,640
needs some configuration, special

32
00:01:17,439 --> 00:01:23,119
toolchains various tweaks

33
00:01:20,640 --> 00:01:23,840
which if you will do on your host

34
00:01:23,119 --> 00:01:26,080
system,

35
00:01:23,840 --> 00:01:26,960
you probably will mess with the

36
00:01:26,080 --> 00:01:29,759
system and

37
00:01:26,960 --> 00:01:30,320
you will break it very, very fast. That's

38
00:01:29,759 --> 00:01:34,079
why

39
00:01:30,320 --> 00:01:35,759
you use containers and you isolate this

40
00:01:34,079 --> 00:01:38,400
development environment in those

41
00:01:35,759 --> 00:01:38,400
containers.

42
00:01:39,119 --> 00:01:45,040
So, also other thing is like when you do

43
00:01:42,159 --> 00:01:46,560
collaborative work or you do trainings

44
00:01:45,040 --> 00:01:49,040
you don't want to waste time

45
00:01:46,560 --> 00:01:49,920
on setting up all these

46
00:01:49,040 --> 00:01:53,119
components,

47
00:01:49,920 --> 00:01:54,720
you just want to pull some image some

48
00:01:53,119 --> 00:01:57,040
environment and just start

49
00:01:54,720 --> 00:01:58,640
development just start compilation and

50
00:01:57,040 --> 00:02:02,320
that's where containers are

51
00:01:58,640 --> 00:02:05,119
very, very helpful. So, to install

52
00:02:02,320 --> 00:02:06,079
Docker in your Linux distribution,

53
00:02:05,119 --> 00:02:10,000
please follow

54
00:02:06,079 --> 00:02:13,120
this link, all the links are

55
00:02:10,000 --> 00:02:14,080
also added to the text section which is

56
00:02:13,120 --> 00:02:17,440
below

57
00:02:14,080 --> 00:02:17,440
this video.

