﻿1
00:00:00,320 --> 00:00:04,160
So, this slide is very interesting and I

2
00:00:02,800 --> 00:00:07,759
would like to

3
00:00:04,160 --> 00:00:10,400
hang on it couple minutes. So, I try

4
00:00:07,759 --> 00:00:11,599
to sketch here open source firmware

5
00:00:10,400 --> 00:00:14,000
ecosystem

6
00:00:11,599 --> 00:00:15,360
and as you can see there are some

7
00:00:14,000 --> 00:00:18,880
patterns here,

8
00:00:15,360 --> 00:00:20,560
first we should divide the firmware

9
00:00:18,880 --> 00:00:22,000
the open source firmware or not open

10
00:00:20,560 --> 00:00:23,840
source theorem because not all these

11
00:00:22,000 --> 00:00:24,800
components are open source firmware it's

12
00:00:23,840 --> 00:00:27,680
like more

13
00:00:24,800 --> 00:00:28,400
enumeration of every possible

14
00:00:27,680 --> 00:00:31,519
firmware

15
00:00:28,400 --> 00:00:32,960
in the ecosystem, some of them are are to

16
00:00:31,519 --> 00:00:36,800
be honest closed.

17
00:00:32,960 --> 00:00:39,520
So, we have um something which can run

18
00:00:36,800 --> 00:00:40,480
on CPU on the left side and something

19
00:00:39,520 --> 00:00:43,680
which is run

20
00:00:40,480 --> 00:00:47,120
either on chips on other chip

21
00:00:43,680 --> 00:00:50,559
inside CPU or on some external

22
00:00:47,120 --> 00:00:51,760
microcontroller or CPU. So, let's start

23
00:00:50,559 --> 00:00:53,680
from the left side

24
00:00:51,760 --> 00:00:55,600
so of course we know coreboot this is

25
00:00:53,680 --> 00:00:56,239
type of firmware it's open source

26
00:00:55,600 --> 00:00:58,719
firmware

27
00:00:56,239 --> 00:01:00,719
we have oreboot maybe you heard about it

28
00:00:58,719 --> 00:01:01,600
it's mostly supported by risk 5

29
00:01:00,719 --> 00:01:04,879
platforms but

30
00:01:01,600 --> 00:01:06,720
it's coreboot written in rust so

31
00:01:04,879 --> 00:01:08,080
they're joking that it's coreboot

32
00:01:06,720 --> 00:01:10,640
without c,

33
00:01:08,080 --> 00:01:12,080
then we have UEFI which as I said

34
00:01:10,640 --> 00:01:14,880
is just the specification

35
00:01:12,080 --> 00:01:16,560
but we have like various

36
00:01:14,880 --> 00:01:20,000
implementations of it,

37
00:01:16,560 --> 00:01:23,280
and we can close in this block many UEFI

38
00:01:20,000 --> 00:01:26,479
firmware implementations which are

39
00:01:23,280 --> 00:01:30,240
in the market, then we have

40
00:01:26,479 --> 00:01:33,759
both host boot and ski boot

41
00:01:30,240 --> 00:01:36,960
which are part of the open power

42
00:01:33,759 --> 00:01:40,159
firmware ecosystem, we have agesa

43
00:01:36,960 --> 00:01:42,799
so here we start

44
00:01:40,159 --> 00:01:43,280
firmware components which are just super

45
00:01:42,799 --> 00:01:47,040
part

46
00:01:43,280 --> 00:01:49,520
of whole firmware for given platform.

47
00:01:47,040 --> 00:01:50,240
So, for example to boot power platform

48
00:01:49,520 --> 00:01:53,680
we need

49
00:01:50,240 --> 00:01:54,159
multiple components, to boot modern

50
00:01:53,680 --> 00:01:55,920


51
00:01:54,159 --> 00:01:57,840
Intel platform we need multiple

52
00:01:55,920 --> 00:02:00,640
components. So, for example

53
00:01:57,840 --> 00:02:00,960
to boot modern Intel, we need coreboot

54
00:02:00,640 --> 00:02:04,479
plus

55
00:02:00,960 --> 00:02:07,520
FSP to boot modern

56
00:02:04,479 --> 00:02:09,119
AMD, we need coreboot plus

57
00:02:07,520 --> 00:02:11,920
agesa.

58
00:02:09,119 --> 00:02:14,000
So, the same thing with power, for power

59
00:02:11,920 --> 00:02:16,319
we need hostboot, skiboot and

60
00:02:14,000 --> 00:02:17,360
other components marked in the same

61
00:02:16,319 --> 00:02:20,239
color.

62
00:02:17,360 --> 00:02:22,879
What else we have, we have romboot,

63
00:02:20,239 --> 00:02:25,840
so romboot is also written in rust and

64
00:02:22,879 --> 00:02:27,200
the idea of this project is to replace

65
00:02:25,840 --> 00:02:30,720
coreboot boot block,

66
00:02:27,200 --> 00:02:33,280
then we have u-boot

67
00:02:30,720 --> 00:02:33,920
which is written in C mostly on

68
00:02:33,280 --> 00:02:36,080
supporting

69
00:02:33,920 --> 00:02:38,319
arms but also many other

70
00:02:36,080 --> 00:02:41,599
architectures.

71
00:02:38,319 --> 00:02:44,879
Then we have components like

72
00:02:41,599 --> 00:02:46,800
grub and SeaBIOS which are the final

73
00:02:44,879 --> 00:02:47,440
stage of the boot process and can be

74
00:02:46,800 --> 00:02:51,120
used

75
00:02:47,440 --> 00:02:54,480
as a final stage of coreboot but also

76
00:02:51,120 --> 00:02:56,319
grub for example can be used with UEFI

77
00:02:54,480 --> 00:02:58,159
so this is like a complexity of this

78
00:02:56,319 --> 00:03:00,800
environment and there is

79
00:02:58,159 --> 00:03:01,360
interesting group that recently growing

80
00:03:00,800 --> 00:03:05,760


81
00:03:01,360 --> 00:03:08,000
very fast and this group is

82
00:03:05,760 --> 00:03:10,239
going back to the time when there was

83
00:03:08,000 --> 00:03:13,840
idea of putting Linux inside

84
00:03:10,239 --> 00:03:15,440
firmware. And on top of all this coreboot,

85
00:03:13,840 --> 00:03:18,000
oreboot, UEFI

86
00:03:15,440 --> 00:03:18,480
like hostboot skiboot all this stuff

87
00:03:18,000 --> 00:03:21,040


88
00:03:18,480 --> 00:03:22,480
it happens that we more often put real

89
00:03:21,040 --> 00:03:25,040
Linux kernel

90
00:03:22,480 --> 00:03:26,959
with various additional tools and

91
00:03:25,040 --> 00:03:27,840
based on that there are those projects

92
00:03:26,959 --> 00:03:30,080
are called

93
00:03:27,840 --> 00:03:32,799
somehow. So, for example we have ???

94
00:03:30,080 --> 00:03:35,120
which is Linux + busybox + some

95
00:03:32,799 --> 00:03:37,840
GPG tools and some additional stuff.

96
00:03:35,120 --> 00:03:38,560
We have LinuxBoot which is Linux kernel

97
00:03:37,840 --> 00:03:43,120
+

98
00:03:38,560 --> 00:03:45,440
some go tools. The rootfs there

99
00:03:43,120 --> 00:03:46,400
is called u-root and the tools related

100
00:03:45,440 --> 00:03:49,200
with that,

101
00:03:46,400 --> 00:03:50,400
then we have petitboot and skiroot

102
00:03:49,200 --> 00:03:53,519
those together

103
00:03:50,400 --> 00:03:55,439
run kernel

104
00:03:53,519 --> 00:03:56,640
and then k exec for their operating

105
00:03:55,439 --> 00:03:59,840
system on

106
00:03:56,640 --> 00:04:00,400
open power and that closes whatever is

107
00:03:59,840 --> 00:04:03,680
running

108
00:04:00,400 --> 00:04:06,720
on the main CPU. Of course there may be other

109
00:04:03,680 --> 00:04:08,879
things here but I didn't

110
00:04:06,720 --> 00:04:11,280
enumerate everything I just wanted to

111
00:04:08,879 --> 00:04:15,280
show how rich this environment.

112
00:04:11,280 --> 00:04:18,560
On the right side of the slide we have

113
00:04:15,280 --> 00:04:21,600
something what is run on

114
00:04:18,560 --> 00:04:24,320
separate CPU the main

115
00:04:21,600 --> 00:04:26,880
CPU of our platform. So, we have something

116
00:04:24,320 --> 00:04:30,479
like management engine which is run

117
00:04:26,880 --> 00:04:33,520
inside PCH, we have PSP, it's

118
00:04:30,479 --> 00:04:36,960
AMD alternative to

119
00:04:33,520 --> 00:04:40,240
ME serve similar functions,

120
00:04:36,960 --> 00:04:40,880
but have like also in modern

121
00:04:40,240 --> 00:04:43,600
platforms

122
00:04:40,880 --> 00:04:44,800
provides additional

123
00:04:43,600 --> 00:04:47,360
features like

124
00:04:44,800 --> 00:04:48,479
memory initialization for example. Then

125
00:04:47,360 --> 00:04:51,840
we have

126
00:04:48,479 --> 00:04:56,800
all this SBE, OTPROM, HBBL,

127
00:04:51,840 --> 00:05:00,080
OCC, the H Codes and you can see how many

128
00:04:56,800 --> 00:05:03,039
components open power

129
00:05:00,080 --> 00:05:03,840
firmware ecosystem use uh to boot the

130
00:05:03,039 --> 00:05:05,840
platform. It's

131
00:05:03,840 --> 00:05:08,400
very complex, it's a lot of

132
00:05:05,840 --> 00:05:09,520
components. So, the trusted computing

133
00:05:08,400 --> 00:05:12,880
base is quite

134
00:05:09,520 --> 00:05:14,639
big, but to be honest this is one of

135
00:05:12,880 --> 00:05:16,560
the most open platforms

136
00:05:14,639 --> 00:05:17,759
already like right now available on the

137
00:05:16,560 --> 00:05:21,919
market.

138
00:05:17,759 --> 00:05:24,400
Then we have BMC which are the

139
00:05:21,919 --> 00:05:25,600
board management

140
00:05:24,400 --> 00:05:28,160
controller

141
00:05:25,600 --> 00:05:29,840
specially dedicated chip on the server

142
00:05:28,160 --> 00:05:32,479
board

143
00:05:29,840 --> 00:05:36,000
which holds typically some Linux so open

144
00:05:32,479 --> 00:05:39,280
BMC is just the Yocto

145
00:05:36,000 --> 00:05:40,160
project which can build um dedicated

146
00:05:39,280 --> 00:05:42,479
Linux,

147
00:05:40,160 --> 00:05:44,160
there is alternative to it call it

148
00:05:42,479 --> 00:05:47,919
u-bmc.

149
00:05:44,160 --> 00:05:51,600
So, u-bmc is got exactly the same

150
00:05:47,919 --> 00:05:58,720
goal to be run on BMC, microcontroller

151
00:05:51,600 --> 00:05:58,720
but the user space is written in go.

