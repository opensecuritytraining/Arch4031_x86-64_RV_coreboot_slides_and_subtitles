1
00:00:00,004 --> 00:00:03,081
Okay,
now that you know how to configure some basic build

2
00:00:03,081 --> 00:00:06,005
for QEMU, let's go back for a while to the

3
00:00:06,005 --> 00:00:07,075
kconfig.

4
00:00:08,014 --> 00:00:11,033
So, when the build is configured,

5
00:00:11,034 --> 00:00:15,036
the kconfig produces two important output files containing the

6
00:00:15,037 --> 00:00:18,025
full set of options selected during the configuration phase.

7
00:00:18,074 --> 00:00:20,096
The first files is .config file,

8
00:00:21,034 --> 00:00:23,034
it is created and saved within the coreboot

9
00:00:23,034 --> 00:00:26,026
directory up on exciting from a new config

10
00:00:26,027 --> 00:00:27,076
text interface.

11
00:00:29,004 --> 00:00:33,006
The .config file gets included by the coreboot main

12
00:00:33,006 --> 00:00:33,506
make file.

13
00:00:34,034 --> 00:00:38,029
Its purpose is to expose the conflict options selected during

14
00:00:38,029 --> 00:00:40,094
the configuration phase to the make file so that the

15
00:00:40,094 --> 00:00:43,076
build system is aware of what is the target configuration.

16
00:00:44,034 --> 00:00:45,054
Based on this information,

17
00:00:45,054 --> 00:00:49,042
the build system knows which source files should be built,

18
00:00:49,048 --> 00:00:51,096
which features should be enabled, etc.

19
00:00:52,094 --> 00:00:55,015
As we can see in this short example,

20
00:00:55,016 --> 00:00:58,006
the build settings are set to yes in short why,

21
00:00:58,061 --> 00:01:02,007
or commented out with is not said string at

22
00:01:02,007 --> 00:01:03,016
the end.

23
00:01:04,004 --> 00:01:06,055
String settings can be set up and a string or

24
00:01:06,056 --> 00:01:07,096
basically left empty.

25
00:01:08,094 --> 00:01:14,032
The kconfig also allows to set various other options

26
00:01:14,032 --> 00:01:16,043
like various other types of options,

27
00:01:16,043 --> 00:01:18,086
like integer or hexadecimal.

28
00:01:20,004 --> 00:01:23,058
Okay,
so now let's see quickly how it looks like for

29
00:01:23,058 --> 00:01:32,009
our QEMU build and let's use capital S command, as
you can see,

30
00:01:32,081 --> 00:01:36,081
the header says that it is automatically generated file and

31
00:01:36,082 --> 00:01:38,064
it shouldn't be edited manually.

32
00:01:38,065 --> 00:01:41,007
So, if we want to make some modifications to do

33
00:01:41,007 --> 00:01:46,061
it for the menu config and you can see the

34
00:01:46,062 --> 00:01:51,081
headers of various sections are named by the sub-menus for

35
00:01:51,081 --> 00:01:52,017
example,

36
00:01:52,017 --> 00:01:55,019
we have the ??? setup and before we have

37
00:01:55,019 --> 00:01:59,081
the main board submenu which is indicated by

38
00:01:59,081 --> 00:02:00,096
the main board header here.

39
00:02:02,024 --> 00:02:03,005
All right here,

40
00:02:03,084 --> 00:02:09,000
we have the general set up here, and

41
00:02:09,001 --> 00:02:11,076
let's study the few options we see.

42
00:02:12,014 --> 00:02:17,061
So, basically each kconfig option which is defined by

43
00:02:17,061 --> 00:02:19,045
by given name.

44
00:02:19,094 --> 00:02:25,078
It's always appended with config  here and the value

45
00:02:25,078 --> 00:02:30,037
set in the many configuration is assigned after the equal

46
00:02:30,037 --> 00:02:30,537
sign.

47
00:02:31,044 --> 00:02:34,078
So, basically this is a coreboot built boolean value which

48
00:02:34,078 --> 00:02:38,091
is set to, "yes" we have a strength local version

49
00:02:38,091 --> 00:02:42,013
which I showed, and it is configured as

50
00:02:42,013 --> 00:02:44,069
empty string, but here we have the CBFS

51
00:02:44,069 --> 00:02:49,042
perfix which indicates what kind of pefix should

52
00:02:49,043 --> 00:02:51,066
stages have.

53
00:02:52,095 --> 00:02:54,086
It is set to fall back string,

54
00:02:55,044 --> 00:03:00,019
you can also see other options like

55
00:03:00,019 --> 00:03:02,094
config any toolchain, which also has to use any other to change

56
00:03:02,094 --> 00:03:06,015
and coreboot one, but it is marked as not set.

57
00:03:07,004 --> 00:03:10,029
Let's try to find some other options like hexadecimal

58
00:03:10,029 --> 00:03:12,046
options or integer options.

59
00:03:13,044 --> 00:03:16,062
So, for example here we have the CBFS size

60
00:03:16,062 --> 00:03:22,045
which is a hexadecimal value or we can have

61
00:03:23,022 --> 00:03:24,007
an integer option.
For example,

62
00:03:24,007 --> 00:03:27,046
the config for UART also is an integer.

63
00:03:28,074 --> 00:03:29,000
Here,

64
00:03:29,000 --> 00:03:32,085
we can also see MAX_ACPI_TABLE_SIZE in kilobytes it's

65
00:03:33,044 --> 00:03:40,006
224.

66
00:03:41,084 --> 00:03:45,052
That's probably are all types of options

67
00:03:45,052 --> 00:03:48,005
that a coreboot with implements and take on.

