﻿1
00:00:00,014 --> 00:00:03,001
Okay, now I will show you how to execute

2
00:00:03,002 --> 00:00:05,036
menu conflict to open the conflagration in the program.

3
00:00:05,094 --> 00:00:10,032
I will also describe what the coreboot many conflict consists

4
00:00:10,032 --> 00:00:13,083
of and what can be configured in each of these

5
00:00:13,083 --> 00:00:16,049
main categories. As I will be going through the menu

6
00:00:16,049 --> 00:00:16,087
conflict

7
00:00:16,087 --> 00:00:18,076
I will explain all these options.

8
00:00:19,024 --> 00:00:19,005
Okay,

9
00:00:19,005 --> 00:00:24,056
so let's execute make menu conflict and to pull display

10
00:00:24,094 --> 00:00:30,074
the configuration menu which is used to set up our

11
00:00:30,074 --> 00:00:33,056
coreboot built, and starting from general set up.

12
00:00:34,014 --> 00:00:40,046
So, here we can see some basic settings for compiler

13
00:00:40,094 --> 00:00:46,055
and other secondary parcels computer stuff

14
00:00:47,014 --> 00:00:48,003
I have some symbols,

15
00:00:48,003 --> 00:00:49,041
option tables,

16
00:00:49,042 --> 00:00:50,093
compression settings,

17
00:00:50,094 --> 00:00:54,042
whether it's included that conflict file into the final

18
00:00:54,042 --> 00:00:55,015
round image,

19
00:00:55,074 --> 00:01:00,096
some blobs configuration, address sanitizer and stuff.

20
00:01:01,044 --> 00:01:04,014
You can also add a boot splash image here,

21
00:01:04,015 --> 00:01:08,056
or where the code should be stashed.

22
00:01:08,056 --> 00:01:09,024
For for example,

23
00:01:09,024 --> 00:01:12,045
for the resume from memory.

24
00:01:13,014 --> 00:01:16,079
Also you can set a local version string here which

25
00:01:16,079 --> 00:01:20,014
will finally appear in the S and BIOS,

26
00:01:20,015 --> 00:01:21,019
for example,

27
00:01:21,002 --> 00:01:23,006
BIOS 0 and the BIOS string,

28
00:01:23,094 --> 00:01:25,086
version and stuff.

29
00:01:27,094 --> 00:01:28,021
Okay,

30
00:01:28,022 --> 00:01:31,048
let's go to the next one which is a

31
00:01:31,049 --> 00:01:32,025
main board,

32
00:01:32,026 --> 00:01:36,062
here is the probably the most often used and most

33
00:01:36,062 --> 00:01:41,064
useful said many because it's always us to set the

34
00:01:41,064 --> 00:01:42,034
main board,

35
00:01:42,035 --> 00:01:46,057
you want to build their the coreboot

36
00:01:46,057 --> 00:01:49,096
for example,

37
00:01:49,096 --> 00:01:51,028


38
00:01:51,029 --> 00:01:57,004
as we can see

39
00:01:57,041 --> 00:02:00,046
we have just selected inhumane board,

40
00:02:01,014 --> 00:02:03,048
you can select some room size for it,

41
00:02:03,049 --> 00:02:05,027
let's say 8MB.

42
00:02:05,084 --> 00:02:10,059
You can select some system power state after the

43
00:02:10,006 --> 00:02:12,006
power has failed.

44
00:02:12,061 --> 00:02:16,025
We can also provide some flash map description file,

45
00:02:16,064 --> 00:02:20,026
which I also described you in previous lessons.

46
00:02:21,004 --> 00:02:24,054
So, here past the path to the FMD file with

47
00:02:24,055 --> 00:02:27,016
describes regions and their sizes and stuff.

48
00:02:28,014 --> 00:02:31,093
Here we may define the size of CBS classified

49
00:02:31,093 --> 00:02:32,071
system.

50
00:02:32,072 --> 00:02:35,084
This typically depends on the size of the BIOS region

51
00:02:35,084 --> 00:02:37,016
on their platforms.

52
00:02:37,054 --> 00:02:41,002
So here it will be let's say 6MB

53
00:02:42,064 --> 00:02:44,095
and let's move to the next category which is

54
00:02:44,095 --> 00:02:51,038
chipset, chipset that contains mainly the associate

55
00:02:51,038 --> 00:02:54,016
specific stuff like the micro architectural stuff,

56
00:02:54,064 --> 00:02:57,027
and here we can see you can enable some options

57
00:02:57,027 --> 00:02:58,048
like hyper threading,

58
00:02:58,049 --> 00:03:03,037
these options like indicate whether the platform can have

59
00:03:03,038 --> 00:03:06,001
a replaceable CPU with Skylink,

60
00:03:06,001 --> 00:03:06,073
CPU cable,

61
00:03:06,074 --> 00:03:08,002
dual core or quad core.

62
00:03:08,021 --> 00:03:11,078
This option is just indicate whether additional micro code files

63
00:03:11,078 --> 00:03:12,046
should be included.

64
00:03:13,094 --> 00:03:18,026
There's also assumptions like for the for the

65
00:03:18,026 --> 00:03:18,008
ME

66
00:03:18,081 --> 00:03:26,026
regions for the read write updates of the ME

67
00:03:26,064 --> 00:03:29,096
whether to perform on MPs sensation by FSP and stuff,

68
00:03:30,074 --> 00:03:34,055
there are the other options like we can enable SGX

69
00:03:34,056 --> 00:03:35,009
the ACPI

70
00:03:35,009 --> 00:03:36,008


71
00:03:36,008 --> 00:03:36,016


72
00:03:36,016 --> 00:03:37,000
timer,

73
00:03:37,001 --> 00:03:41,066
legacy pete timer and other stuff.

74
00:03:44,024 --> 00:03:49,045
Also, here may set main future control register,

75
00:03:49,045 --> 00:03:51,095
log bit and virtualization,

76
00:03:52,084 --> 00:03:56,055
here recommends also select which Microsoft file sharing protocol,

77
00:03:56,055 --> 00:04:01,063
whether we generated from the available tree or actually called

78
00:04:01,063 --> 00:04:02,076
external binaries.

79
00:04:02,081 --> 00:04:05,042
We can also add some five descriptor

80
00:04:05,042 --> 00:04:07,046
If we have one image,

81
00:04:07,084 --> 00:04:11,046
you can also apply and make cleaner if you want.

82
00:04:12,014 --> 00:04:19,013
We can also select which bootblock to load,

83
00:04:19,014 --> 00:04:22,033
which other stage actually to load depending on

84
00:04:22,033 --> 00:04:24,096
the symbols are always we can load the fallback,

85
00:04:26,034 --> 00:04:31,047
the fallback means the prefix which is appended

86
00:04:31,048 --> 00:04:35,076
to the the stage name in the CBFS. We can

87
00:04:35,076 --> 00:04:37,021
also protect some flash regions,

88
00:04:37,021 --> 00:04:40,053
like we can lock ME TXE region

89
00:04:40,054 --> 00:04:42,036
or use the preset values.

90
00:04:42,037 --> 00:04:46,091
So, basically it says whether the FD tool which is

91
00:04:46,091 --> 00:04:50,058
available in coreboot, should log the flash descriptor which

92
00:04:50,058 --> 00:04:52,026
you provide here in the file.

93
00:04:54,014 --> 00:04:57,075
So, next section of the devices section, and here we

94
00:04:57,075 --> 00:05:02,025
mainly have options for the graphics device and piece of

95
00:05:02,025 --> 00:05:03,023
extra devices.

96
00:05:03,024 --> 00:05:07,046
So, basically we select here which kind of

97
00:05:07,084 --> 00:05:11,068
operation we can performed to utilize the graphics,

98
00:05:11,068 --> 00:05:13,026
like we can use the open source sleep,

99
00:05:13,084 --> 00:05:14,033
JFX

100
00:05:14,033 --> 00:05:15,085
innit library,

101
00:05:16,034 --> 00:05:20,007
which is right in spark ADA language, or we can

102
00:05:20,007 --> 00:05:25,072
alternatively around. VGA optionals around the GOP driver from FSB

103
00:05:25,097 --> 00:05:27,046
or either do nothing.

104
00:05:29,014 --> 00:05:33,091
You can also set what the frame buffer should be

105
00:05:33,091 --> 00:05:36,064
like whether we want to show the graphical book,

106
00:05:36,064 --> 00:05:37,046
splash and stuff.

107
00:05:38,074 --> 00:05:39,036
Also,

108
00:05:39,036 --> 00:05:42,006
there is a new option which allows us to configure

109
00:05:42,006 --> 00:05:47,029
the bassmaster pits um incorporate for the devices,

110
00:05:47,003 --> 00:05:51,024
you can also enable some sex is power management features like

111
00:05:51,024 --> 00:05:53,057
??? clock ASPM sub states

112
00:05:53,057 --> 00:05:57,014
we can also enable hot black and then we see

113
00:05:57,015 --> 00:05:59,019
how many buses we want to reserve and how much

114
00:05:59,019 --> 00:06:01,046
memory you want to reserve for these buses and stuff.

115
00:06:02,084 --> 00:06:06,019
We can also override the PCI subsystem vendor and the device

116
00:06:06,019 --> 00:06:09,005
ID and include VGA BIOS images.

117
00:06:10,014 --> 00:06:12,011
You can include VBT binary,

118
00:06:12,012 --> 00:06:14,055
cannot additional

119
00:06:15,044 --> 00:06:17,002
VGA images as well,

120
00:06:17,009 --> 00:06:20,004
if we have more than one graphics controller in the

121
00:06:20,004 --> 00:06:20,066
system.

122
00:06:21,074 --> 00:06:22,098
Generic drivers,

123
00:06:22,099 --> 00:06:28,029
generic drivers described the options that are available for

124
00:06:28,029 --> 00:06:33,005
the platform from the generic corporate drivers which are

125
00:06:33,006 --> 00:06:36,016
common on across many platforms.

126
00:06:36,054 --> 00:06:42,015
You can enable some event lock in the

127
00:06:42,016 --> 00:06:47,053
flash, you can enable some mediated data store,

128
00:06:47,054 --> 00:06:52,053
which is a data stor in the SPI flash which

129
00:06:52,054 --> 00:06:55,015
usess SMI interacts to

130
00:06:55,064 --> 00:07:01,072
communicate to the store. You can also enable some other additional

131
00:07:01,072 --> 00:07:04,026
models like Oxford OXPCIe,

132
00:07:04,064 --> 00:07:08,006
you can support for VPD,

133
00:07:08,065 --> 00:07:14,038
VPD is structure defined by their ??? project,

134
00:07:14,039 --> 00:07:19,024
which is just widely to start some configuration for

135
00:07:19,024 --> 00:07:20,006
the platforms,

136
00:07:20,007 --> 00:07:20,087
Chromebooks.

137
00:07:21,019 --> 00:07:26,046
you can include some serial numbers in CBFS.

138
00:07:26,047 --> 00:07:33,045
Here we also may configure how we should include that FSP

139
00:07:33,045 --> 00:07:34,001
binaries,

140
00:07:34,001 --> 00:07:37,053
you can either use the have sp repo or attend

141
00:07:37,054 --> 00:07:40,008
some external FSB binaries,

142
00:07:40,008 --> 00:07:42,026
which we provide the path to.

143
00:07:43,034 --> 00:07:45,064
We can also enable the logo,

144
00:07:45,065 --> 00:07:48,056
which is a BMP file past to the FSB to

145
00:07:48,056 --> 00:07:50,096
be displayed during the synchronization,

146
00:07:51,044 --> 00:07:54,082
may initialize the PS2 keyboard and other stuff as

147
00:07:54,082 --> 00:07:55,006
well.

148
00:07:56,084 --> 00:07:58,046
Security,

149
00:07:58,084 --> 00:08:01,068
this is probably the most interesting

150
00:08:01,069 --> 00:08:02,026
menu.

151
00:08:03,024 --> 00:08:08,025
So it consists now for a few options here,

152
00:08:08,074 --> 00:08:12,035
like verified boot in short, vboot.

153
00:08:12,074 --> 00:08:17,005
This is a library developed by the chromium ChromeOS

154
00:08:17,005 --> 00:08:23,023
project for the sake of secure boot of the

155
00:08:23,023 --> 00:08:28,007
Chromebook devices and it just does the cryptographic

156
00:08:28,071 --> 00:08:31,015
verification of various firwarwe components.

157
00:08:31,074 --> 00:08:36,004
I won't go into much details because it's very

158
00:08:36,041 --> 00:08:37,027
complex

159
00:08:37,027 --> 00:08:41,036
ME and has a lot of various options which most

160
00:08:41,036 --> 00:08:44,005
of them is not using our regular platforms that are

161
00:08:44,005 --> 00:08:45,036
not Chromebooks.

162
00:08:46,024 --> 00:08:49,036
We can also enable trusted platform module here,

163
00:08:49,094 --> 00:08:53,015
for example we have available option for TPM 2.0,

164
00:08:53,074 --> 00:08:56,096
can enable mirrored boot and and stuff.

165
00:08:58,054 --> 00:09:01,091
The ??? is quite simple because the only option

166
00:09:01,091 --> 00:09:06,016
right now here is adoption to clear all freed DRAM

167
00:09:06,016 --> 00:09:10,056
on regular boot, it is very useful for example if

168
00:09:10,056 --> 00:09:16,007
we have some secrets in the memory, especially when

169
00:09:16,007 --> 00:09:19,049
we have for example the trusted execution technology which enforces

170
00:09:19,049 --> 00:09:22,072
us to clear the memory if it detects any

171
00:09:22,072 --> 00:09:29,006
secrets may be leaked and one they wanted just for

172
00:09:29,084 --> 00:09:30,056
security.

173
00:09:30,094 --> 00:09:34,024
We have also added directly support many which allows us

174
00:09:34,024 --> 00:09:39,045
to include the ACMS in the image enables

175
00:09:39,045 --> 00:09:40,007
verbose logging

176
00:09:40,007 --> 00:09:43,071
and can basically execute the TXT driver and implemented

177
00:09:44,084 --> 00:09:45,078
in coreboot.

178
00:09:45,079 --> 00:09:49,013
We have also SMI transform inter integrated to

179
00:09:49,013 --> 00:09:54,025
coreboot. So, you may select this option and basically mm

180
00:09:54,064 --> 00:09:58,005
provide the path to SDM binary which would be integrated

181
00:09:58,005 --> 00:10:01,016
and loaded into SMM.

182
00:10:02,084 --> 00:10:07,069
We have also the boot protection menu, which allows

183
00:10:07,069 --> 00:10:10,005
us to set the SPI flash log beats which are

184
00:10:10,005 --> 00:10:16,086
specific to given controller of or SPI flash ship. Council,

185
00:10:17,034 --> 00:10:22,009
it's basically menu for various the debugging options are like

186
00:10:22,001 --> 00:10:25,026
where the coreboot console should be out with like

187
00:10:25,094 --> 00:10:27,075
from which stages,

188
00:10:28,043 --> 00:10:30,095
what kind of serial port to

189
00:10:31,034 --> 00:10:31,066


190
00:10:32,014 --> 00:10:34,002
serial port or other consoles,

191
00:10:34,021 --> 00:10:37,012
Do you want to enable, like we can have a speaker

192
00:10:37,012 --> 00:10:41,056
modem or network console over compatible adapter,

193
00:10:41,094 --> 00:10:44,074
or you can have SPI flash consultant which just write

194
00:10:44,075 --> 00:10:45,096
the SPI flash there,

195
00:10:46,071 --> 00:10:48,009
and the output of the commands,

196
00:10:48,041 --> 00:10:52,053
the output of the corporate locks,

197
00:10:52,054 --> 00:10:54,085
we can also configure the debug level,

198
00:10:55,024 --> 00:10:59,018
you can compute the post codes which can be for

199
00:10:59,018 --> 00:11:03,076
example written to PCI interface for us with a devboot postcard.

200
00:11:05,044 --> 00:11:11,029
System tables are practically ME for generating SMBIOS

201
00:11:11,029 --> 00:11:12,004
tables

202
00:11:12,005 --> 00:11:14,064
some platforms may also have the

203
00:11:14,064 --> 00:11:14,082
PIRQ

204
00:11:14,082 --> 00:11:15,012


205
00:11:15,012 --> 00:11:19,004
Table or mp table for generation and other stuff.

206
00:11:19,041 --> 00:11:23,006
You basically can set here what the SMBIOS should

207
00:11:23,006 --> 00:11:27,038
be populated with. Payload,

208
00:11:27,039 --> 00:11:32,004
very important

209
00:11:32,054 --> 00:11:36,023
submenu because here may choose the final application which is

210
00:11:36,023 --> 00:11:40,085
loaded by coreboat and as I have previously

211
00:11:41,024 --> 00:11:44,094
described we have various payloads to choose like Tianocore

212
00:11:44,094 --> 00:11:45,053
payloads,

213
00:11:45,054 --> 00:11:45,097
Linux

214
00:11:45,097 --> 00:11:46,092
Linux boot,

215
00:11:46,093 --> 00:11:47,085
Sebelius,

216
00:11:47,086 --> 00:11:49,012
Yabits,

217
00:11:49,013 --> 00:11:55,056
u-boot, GRUB2 payload or another executable payload.

218
00:11:56,094 --> 00:12:01,091
We can also have various other payload specific options here

219
00:12:01,091 --> 00:12:03,096
like some SeaBIOS configuration.

220
00:12:04,064 --> 00:12:08,037
But if we choose for example the Tianacore,

221
00:12:08,038 --> 00:12:12,026
we will have Tianacore specific options like which kind

222
00:12:12,026 --> 00:12:13,011
of Tianacore

223
00:12:13,011 --> 00:12:14,066
payload flavor we want,

224
00:12:15,044 --> 00:12:18,095
which target architecture or release debug build and stuff.

225
00:12:20,024 --> 00:12:20,077
Also,

226
00:12:20,077 --> 00:12:25,006
we may select to build a PXE ROM.

227
00:12:25,061 --> 00:12:29,067
You can use a PXE open source implementation to build

228
00:12:29,067 --> 00:12:29,096
it,

229
00:12:30,074 --> 00:12:34,078
or we may add some secondary payloads which also described

230
00:12:34,079 --> 00:12:38,061
earlier core implement as tent here

231
00:12:38,061 --> 00:12:41,085
we may even select the ???.

232
00:12:43,044 --> 00:12:43,069
Okay,

233
00:12:43,069 --> 00:12:46,019
and the last one is the debugging menu,

234
00:12:46,002 --> 00:12:52,015
which allows us to enable even more messages to be

235
00:12:52,015 --> 00:13:00,001
displayed besides those which are printed by default

236
00:13:00,011 --> 00:13:01,006
in the coreboot

237
00:13:01,007 --> 00:13:04,035
??? which is configured by the debug and the

238
00:13:04,035 --> 00:13:05,005
console ME.

239
00:13:05,074 --> 00:13:08,047
And it allows us to offer some verbal messages.

240
00:13:08,048 --> 00:13:09,023
For example,

241
00:13:09,023 --> 00:13:11,087
we may have a SMI debugging, when we select this

242
00:13:11,087 --> 00:13:12,046
option

243
00:13:12,084 --> 00:13:15,084
you can have some verbose CBFS

244
00:13:15,085 --> 00:13:17,056
debug messages.

245
00:13:18,004 --> 00:13:21,055
So, whenever and access to CBFS is done,

246
00:13:22,024 --> 00:13:25,075
the council will print and additional debugging messages

247
00:13:25,076 --> 00:13:30,028
when for example scanning the CBFS for the requested

248
00:13:30,028 --> 00:13:30,067
file.

249
00:13:30,068 --> 00:13:35,086
You can also display more information about FSP like here,

250
00:13:36,024 --> 00:13:41,008
you can also display firmware ingredient version

251
00:13:41,008 --> 00:13:43,046
information from the FSP and stuff.

252
00:13:44,044 --> 00:13:45,038
For example,

253
00:13:45,039 --> 00:13:50,006
we may also configured image for the debug EM100

254
00:13:50,006 --> 00:13:55,067
emulator or for example compile debug code ADA sources,

255
00:13:55,067 --> 00:13:59,031
which is especially useful for ???, to leave the ???

256
00:13:59,031 --> 00:13:59,076
in it.

257
00:14:00,074 --> 00:14:02,057
And we have for example,

258
00:14:02,057 --> 00:14:05,008
SPI flash debug messages, when we want to debug

259
00:14:05,009 --> 00:14:10,035
SPI flash access for the chip set.

260
00:14:12,064 --> 00:14:13,026
Okay,

261
00:14:13,027 --> 00:14:19,036
I will leave different investigation of these options

262
00:14:19,084 --> 00:14:20,056
to you.

263
00:14:21,014 --> 00:14:26,087
Just a quick reminder,

264
00:14:26,088 --> 00:14:29,088
you may use the slash button to search for

265
00:14:29,088 --> 00:14:35,011
a configuration parameter or press the question mark to for

266
00:14:35,011 --> 00:14:35,053
example,

267
00:14:35,053 --> 00:14:38,036
display the help of give an option to give you

268
00:14:38,094 --> 00:14:42,068
moreover viewing details, what to give an option really does

269
00:14:42,069 --> 00:14:43,076
when selected.

270
00:14:44,034 --> 00:14:44,068
Note

271
00:14:44,068 --> 00:14:46,011
that, not

272
00:14:46,012 --> 00:14:52,048
all options may have some help.

273
00:14:52,049 --> 00:14:53,025


274
00:14:54,024 --> 00:14:54,044


275
00:14:54,094 --> 00:14:56,011
For example,

276
00:14:56,094 --> 00:14:58,016
CPU_AMD_PI

277
00:14:58,064 --> 00:15:02,013
and as you can see this is an internal

278
00:15:02,013 --> 00:15:06,056
option which is selected by some given CPUs and micro

279
00:15:06,056 --> 00:15:11,038
architectures and this basically are not mean to

280
00:15:11,038 --> 00:15:13,084
be selected by menu configs.

281
00:15:13,084 --> 00:15:15,095
So, they have no help

282
00:15:16,084 --> 00:15:17,046
section.

283
00:15:20,064 --> 00:15:21,001
Okay,

284
00:15:21,011 --> 00:15:25,055
so that would conclude the menu config

285
00:15:28,023 --> 00:15:34,009
section. So, please try it

286
00:15:34,009 --> 00:15:35,067
yourself,

287
00:15:35,068 --> 00:15:38,008
go through the menu and try to explore as much

288
00:15:38,008 --> 00:15:41,046
as you can to learn how it looks like.

