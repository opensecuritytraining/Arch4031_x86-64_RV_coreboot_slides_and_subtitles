﻿1
00:00:00,084 --> 00:00:04,009
So, what the CBFS really is? The CBFS

2
00:00:04,001 --> 00:00:06,036
stands for coreboot File System.

3
00:00:06,084 --> 00:00:09,086
It has a similar concept to file system,

4
00:00:10,024 --> 00:00:12,066
but in fact it is not one of them.

5
00:00:13,004 --> 00:00:17,011
It's rather a scheme for managing independent binary data,

6
00:00:17,012 --> 00:00:20,015
so called files located in a single RAM image.

7
00:00:21,003 --> 00:00:23,028
It is meant to be flashed on RAM chips,

8
00:00:23,054 --> 00:00:27,005
its main property is that it's read only, it

9
00:00:27,005 --> 00:00:30,089
should be assembled off target from some data trunks into

10
00:00:30,089 --> 00:00:32,016
a single ROM image.

11
00:00:32,074 --> 00:00:35,056
Then, it can be flashed on a target from chip,

12
00:00:36,024 --> 00:00:40,011
utility that is capable of assembling such image is called

13
00:00:40,011 --> 00:00:43,056
the CBFS 2, and will be discussed in next lesson.

14
00:00:44,024 --> 00:00:44,098
Also,

15
00:00:44,099 --> 00:00:48,002
there is no such thing as directory structure even when

16
00:00:48,002 --> 00:00:50,066
sometimes it may look like this.

17
00:00:52,024 --> 00:00:56,035
So, the CBFS tool is a management utility for managing

18
00:00:56,035 --> 00:00:58,045
the CPFS for ??? RAM images.

19
00:00:58,094 --> 00:01:01,046
It is used in coreboot build process to assemble the

20
00:01:01,046 --> 00:01:02,056
wrong image structure,

21
00:01:03,004 --> 00:01:05,058
but it can also be used manually to print image

22
00:01:05,058 --> 00:01:08,076
structure and new flg or extract existing file.

23
00:01:09,054 --> 00:01:11,057
It is built as a part of the coreboot build

24
00:01:11,057 --> 00:01:14,046
process and located and build CBFS tool.

25
00:01:15,024 --> 00:01:19,022
So, let's look how it looks like after

26
00:01:19,022 --> 00:01:19,086
the build process.

27
00:01:22,044 --> 00:01:24,094
So, as we have seen earlier,

28
00:01:25,044 --> 00:01:30,022
the CBFS has to print the carbons image contents at

29
00:01:30,023 --> 00:01:31,056
the end of the build process.

30
00:01:31,094 --> 00:01:35,022
So, we see a certain files which are named

31
00:01:35,022 --> 00:01:38,004
in the most left call.

32
00:01:38,053 --> 00:01:42,046
We have this stage record like ROM stage RAM stage,

33
00:01:42,084 --> 00:01:45,026
post car, payload and to boot block.

34
00:01:46,014 --> 00:01:47,026
Besides these files,

35
00:01:47,026 --> 00:01:50,008
we have also some necessary files like DSDT

36
00:01:50,008 --> 00:01:53,016
which is an ACPI table.

37
00:01:53,054 --> 00:01:54,086
We have the symbols layout,

38
00:01:55,034 --> 00:02:00,046
binary father which contains runtime options, we have some basic

39
00:02:00,047 --> 00:02:02,018
coreboot files like the config,

40
00:02:02,019 --> 00:02:03,061
minimal conflict,

41
00:02:03,062 --> 00:02:08,009
the revision of the coreboot repository and the payload config

42
00:02:08,009 --> 00:02:09,016
revision files.

43
00:02:10,085 --> 00:02:13,069
The offset column explains the offset from the start of

44
00:02:13,007 --> 00:02:16,042
the image to the beginning of the file.

45
00:02:18,004 --> 00:02:20,046
Each of the files can have different types,

46
00:02:20,084 --> 00:02:23,061
we have various types like stages, raw files,

47
00:02:23,062 --> 00:02:24,008
cmos layout,

48
00:02:24,081 --> 00:02:25,058
simple,

49
00:02:25,059 --> 00:02:29,036
executive and linking file, boot block

50
00:02:30,004 --> 00:02:34,047
and there are also many other file types like SPD

51
00:02:34,048 --> 00:02:35,052
for the serial presence,

52
00:02:35,052 --> 00:02:37,005
detect on the

53
00:02:37,051 --> 00:02:39,031
D memory modules

54
00:02:39,032 --> 00:02:40,048
and for example,

55
00:02:40,049 --> 00:02:43,072
also there is a separate file type for Intel

56
00:02:43,072 --> 00:02:44,018
FSP

57
00:02:44,018 --> 00:02:45,001
binaries,

58
00:02:45,002 --> 00:02:45,076
etc.

59
00:02:46,074 --> 00:02:50,018
Of course each file has it on the size which

60
00:02:50,018 --> 00:02:52,066
is presented in the size column,

61
00:02:53,024 --> 00:02:57,008
in the last column indicates the compression applied to given

62
00:02:57,008 --> 00:02:57,045
file.

63
00:02:57,088 --> 00:03:04,052
Currently there is nothing compressed although the compressed

64
00:03:04,052 --> 00:03:05,066
stages are for example,

65
00:03:05,066 --> 00:03:07,019
RAM stage and payload

66
00:03:07,036 --> 00:03:11,055
they are typically compressed but it is not indicated.

67
00:03:23,014 --> 00:03:26,054
So, now let's review the CBFS and it's a

68
00:03:26,055 --> 00:03:27,025
common line.

69
00:03:27,084 --> 00:03:31,012
So, the CBFS tool after the build process, is located

70
00:03:31,012 --> 00:03:32,085
at the build CBFS tool.

71
00:03:33,044 --> 00:03:35,077
It has quite really good common line help,

72
00:03:35,078 --> 00:03:37,059
so all possible comments,

73
00:03:37,059 --> 00:03:40,046
parameters and types can be found directly there.

74
00:03:41,013 --> 00:03:44,099
The syntax is simple, simply pass the ROM image name

75
00:03:44,099 --> 00:03:46,038
as the first argument,

76
00:03:46,052 --> 00:03:50,053
then the command in the parameters at the end. The

77
00:03:50,053 --> 00:03:50,081
-vflag

78
00:03:50,081 --> 00:03:53,047
can of course be used to enable some

79
00:03:53,047 --> 00:03:54,045
fair boot output.

80
00:03:55,077 --> 00:03:59,093
The most commonly used comments are probably the print, extract,

81
00:04:00,014 --> 00:04:03,005
added at stage, and add payload.

82
00:04:03,094 --> 00:04:06,096
So, let's have a look how the help looks like.

83
00:04:09,024 --> 00:04:11,093
I would not just execute the CBFS tool from the

84
00:04:11,094 --> 00:04:14,022
build CBFS directory age

85
00:04:14,022 --> 00:04:22,016
flag command, and so we have some usage examples with

86
00:04:22,017 --> 00:04:26,066
options explanatory, and

87
00:04:26,067 --> 00:04:31,072
the first command is added, which simply as a given component,

88
00:04:31,073 --> 00:04:36,021
given regions. The region can be a F map-flash map

89
00:04:36,021 --> 00:04:40,015
region. F indicates the file and name and T type.

90
00:04:40,094 --> 00:04:45,028
We can also optionally append hash, apply compression,

91
00:04:45,045 --> 00:04:49,074
defines some base urges or alignment but yes,

92
00:04:50,024 --> 00:04:51,008
these are mutual excluding.

93
00:04:51,008 --> 00:04:53,026
So, you have to define only one.

94
00:04:53,094 --> 00:04:56,056
We can also pass some padding size

95
00:04:56,094 --> 00:04:59,074
and for example,

96
00:04:59,074 --> 00:05:03,089
we can specify whether this function is execute in place,

97
00:05:03,009 --> 00:05:09,055
in case we have some FSP or program stage,

98
00:05:09,094 --> 00:05:12,048
and we can define

99
00:05:12,048 --> 00:05:13,009
for example,

100
00:05:13,009 --> 00:05:16,026
some other Intel specific options.

101
00:05:18,064 --> 00:05:21,002
We have also a special common lines to add the

102
00:05:21,002 --> 00:05:23,046
payload stage or a flat binary,

103
00:05:24,044 --> 00:05:30,038
these are almost the same but take different arguments because

104
00:05:30,039 --> 00:05:36,041
the different file types with different arguments

105
00:05:36,042 --> 00:05:38,002
for example,

106
00:05:38,063 --> 00:05:41,001
payload has some specific arguments.

107
00:05:41,001 --> 00:05:41,069
For example,

108
00:05:41,069 --> 00:05:44,005
we can add some command lines and ???,

109
00:05:44,094 --> 00:05:47,028
we can also apply compression,

110
00:05:47,028 --> 00:05:49,065
define a base address but not alignment.

111
00:05:52,014 --> 00:05:53,015
For example,

112
00:05:53,016 --> 00:05:58,025
the stage also can have some compression.

113
00:05:58,025 --> 00:05:59,019
The base address,

114
00:05:59,074 --> 00:06:01,063
we can ignore certain sections,

115
00:06:01,064 --> 00:06:07,006
for example ROM stages is added to CBFS without certain sections,

116
00:06:07,054 --> 00:06:11,004
you can define the alignment and some page size, also

117
00:06:11,041 --> 00:06:16,007
executive place, etc. At integer,

118
00:06:17,003 --> 00:06:20,045
this is a simple comment that as a 64 bit

119
00:06:20,045 --> 00:06:22,046
integr to the CBFS.

120
00:06:22,084 --> 00:06:27,004
It is often used to add some SeaBIOS runtime configuration,

121
00:06:27,005 --> 00:06:30,051
we just passed the integer after the -i

122
00:06:30,052 --> 00:06:35,066
flag and give the value.

123
00:06:38,034 --> 00:06:42,033
There is also some add master header which is a

124
00:06:42,034 --> 00:06:49,036
legacy CBFS header instance, you can remove certain

125
00:06:49,074 --> 00:06:54,092
files from given regions, you can compact the CBFS.

126
00:06:54,092 --> 00:06:58,097
So, if we have some holes after extracted or removed

127
00:06:58,098 --> 00:06:59,078
files.

128
00:06:59,079 --> 00:07:03,086
We can compact damage again to save some space.

129
00:07:05,054 --> 00:07:08,065
We can create and duplicate some copy of CBFS

130
00:07:08,065 --> 00:07:10,055
instance in the flesh map.

131
00:07:11,004 --> 00:07:14,051
We can create some legacy ROM file with CBFS

132
00:07:14,051 --> 00:07:15,054
monster header.

133
00:07:16,034 --> 00:07:19,085
You can create some new style also partitioned firmware images

134
00:07:19,086 --> 00:07:21,026
with flasmap.

135
00:07:22,044 --> 00:07:23,036


136
00:07:23,074 --> 00:07:28,057
We can also locate a file in the CBFS

137
00:07:28,057 --> 00:07:31,045
or find a place for a file of given size.

138
00:07:31,084 --> 00:07:35,025
We can print the layout of the ROM,

139
00:07:35,094 --> 00:07:40,016
it will print us at the flushmap partitions and

140
00:07:40,017 --> 00:07:44,031
print commenters for printing the CBFS content.

141
00:07:44,031 --> 00:07:47,056
So, we have to define their freshman region

142
00:07:48,004 --> 00:07:51,046
with containing the CBFS,

143
00:07:52,089 --> 00:07:59,007
we can extract certain files and certain file types

144
00:07:59,007 --> 00:08:03,011
in CBFS require also passing the architecture in order to

145
00:08:03,012 --> 00:08:05,026
correctly extract the file.

146
00:08:05,064 --> 00:08:06,063
For example,

147
00:08:06,074 --> 00:08:09,031
if we want to extract a payload or a stage

148
00:08:09,032 --> 00:08:12,015
we have to pass the architecture in order to correctly

149
00:08:12,094 --> 00:08:15,006
extract the executable.

150
00:08:15,084 --> 00:08:19,012
It can also simply write and read a certain

151
00:08:19,012 --> 00:08:21,007
flashmap regions.

152
00:08:21,008 --> 00:08:25,054
We can truncate certain regions and expand them as

153
00:08:25,054 --> 00:08:25,096
well.

154
00:08:27,034 --> 00:08:30,002
And at the end of the help command, we have

155
00:08:30,002 --> 00:08:35,008
some explanations about the values that have to be

156
00:08:35,008 --> 00:08:36,005
passed to certain flags,

157
00:08:36,005 --> 00:08:39,029
like offset architectures and types. Here

158
00:08:39,029 --> 00:08:43,001
you can see the types of all CBFS files which

159
00:08:43,001 --> 00:08:46,006
are interpreted.

160
00:08:49,064 --> 00:08:49,087
Okay,

161
00:08:49,087 --> 00:08:55,025
now let's exercise certain basic CBFS tool commands.

162
00:08:55,085 --> 00:08:56,065
Like print,

163
00:08:56,065 --> 00:08:57,031
extract,

164
00:08:57,032 --> 00:08:58,001
add files,

165
00:08:58,001 --> 00:08:59,046
remove files, etc.

166
00:09:02,054 --> 00:09:05,096
Now, I will record everything for you so don't worry,

167
00:09:06,048 --> 00:09:08,018
if you're not keeping up,

168
00:09:08,018 --> 00:09:11,061
you will have a chance to replay everything

169
00:09:11,061 --> 00:09:17,008
and exercise yourself. So, let's change to the build

170
00:09:17,008 --> 00:09:25,052
directory and execute the first most important command

171
00:09:25,052 --> 00:09:26,065
which is print.

172
00:09:27,054 --> 00:09:30,002
We passed the coreboot ROM which is also a place

173
00:09:30,002 --> 00:09:33,096
in the build directory, that execute print.

174
00:09:34,054 --> 00:09:38,069
As we can see, it contains all our files has

175
00:09:38,069 --> 00:09:40,096
presented area after the build process.

176
00:09:42,014 --> 00:09:42,034


177
00:09:42,084 --> 00:09:43,061
And yes,

178
00:09:43,008 --> 00:09:47,036
let's try some basic comments like extract

179
00:09:49,024 --> 00:09:49,045


180
00:09:52,034 --> 00:09:54,023
and maybe this extract

181
00:09:54,024 --> 00:09:55,036
config file.

182
00:09:56,094 --> 00:09:57,056


183
00:10:02,014 --> 00:10:02,034


184
00:10:02,084 --> 00:10:03,005


185
00:10:03,084 --> 00:10:04,005


186
00:10:05,074 --> 00:10:11,023
It has been extracted successfully, and let's just dump the

187
00:10:11,024 --> 00:10:11,066
file.

188
00:10:12,014 --> 00:10:12,035


189
00:10:13,094 --> 00:10:14,098
And as you can see,

190
00:10:15,018 --> 00:10:16,076
the only option that was selected,

191
00:10:17,034 --> 00:10:21,015
Non default option was our main board selection.

192
00:10:22,094 --> 00:10:23,014


193
00:10:24,005 --> 00:10:28,016
And now, let's try for example to remove certain

194
00:10:30,014 --> 00:10:30,035


195
00:10:30,084 --> 00:10:36,081
files like-let's remove the config actually.

196
00:10:38,064 --> 00:10:40,065


197
00:10:41,004 --> 00:10:44,021
And now print the content again to ensure it has

198
00:10:44,021 --> 00:10:45,056
really been removed.

199
00:10:46,003 --> 00:10:47,005


200
00:10:47,044 --> 00:10:49,096
And we see and empty space,

201
00:10:50,044 --> 00:10:52,075
in the place where the config file was.

202
00:10:53,054 --> 00:10:56,016
You may notice that the size is different right

203
00:10:56,016 --> 00:11:01,093
now, because each file contains a header that specifies the

204
00:11:01,093 --> 00:11:03,046
start and the end of the file.

205
00:11:04,024 --> 00:11:09,004
So, it is some overhead when we place a file,

206
00:11:09,022 --> 00:11:11,008
but when the place is empty,

207
00:11:11,081 --> 00:11:12,036


208
00:11:12,037 --> 00:11:15,016
the size of emptiness is wider.

209
00:11:15,094 --> 00:11:16,045


210
00:11:17,034 --> 00:11:17,093


211
00:11:17,093 --> 00:11:21,055
And now, we can add some file right.

212
00:11:22,084 --> 00:11:23,065
Maybe

213
00:11:25,094 --> 00:11:26,026


214
00:11:26,094 --> 00:11:29,076
let's write some custom strength,

215
00:11:31,094 --> 00:11:34,008
let's say hello world,

216
00:11:35,047 --> 00:11:38,023
it depended to my file.

217
00:11:40,054 --> 00:11:40,075


218
00:11:42,007 --> 00:11:43,037
And then ???

219
00:11:43,037 --> 00:11:47,016
CBFS tool or coreboot RAM to add the file.

220
00:11:48,014 --> 00:11:51,037
So, first we need to specify which file we want

221
00:11:51,037 --> 00:11:53,086
to add, its name,

222
00:11:54,044 --> 00:11:57,056
let's say it will be my file.

223
00:11:59,014 --> 00:12:03,066
And that's probably not enough.

224
00:12:04,024 --> 00:12:04,077


225
00:12:04,078 --> 00:12:09,039
Because, we have to type the type of the file

226
00:12:09,044 --> 00:12:13,005
so it should be a raw file and now everything is

227
00:12:13,005 --> 00:12:17,036
correct. ???

228
00:12:18,074 --> 00:12:21,095
and we have the myfile and the CBFS.

229
00:12:23,094 --> 00:12:26,089
We can also apply compression to the file.

230
00:12:26,009 --> 00:12:28,075
So, let's say that

231
00:12:29,034 --> 00:12:29,066


232
00:12:30,044 --> 00:12:38,034
my file will be compressed and the compression is indicated by

233
00:12:38,034 --> 00:12:39,074
flag -c

234
00:12:40,009 --> 00:12:41,093
And we can use  ZMA

235
00:12:41,093 --> 00:12:42,001


236
00:12:42,001 --> 00:12:42,086
compression,

237
00:12:45,084 --> 00:12:46,005


238
00:12:46,094 --> 00:12:52,006
And it fails because the file size is too small.

239
00:12:53,004 --> 00:12:53,045


240
00:12:55,054 --> 00:12:58,066
We need to have a larger file.

241
00:13:01,074 --> 00:13:02,025
Well,

242
00:13:03,034 --> 00:13:06,049
where my ??? CBFS actually. Let's try

243
00:13:06,049 --> 00:13:06,096
this out,

244
00:13:09,024 --> 00:13:09,095


245
00:13:12,034 --> 00:13:12,055


246
00:13:14,014 --> 00:13:19,051
so let's include the CBFS tool inside coreboot.

247
00:13:20,034 --> 00:13:20,066


248
00:13:21,004 --> 00:13:21,045


249
00:13:22,074 --> 00:13:23,066
Yes,

250
00:13:24,013 --> 00:13:24,096
it succeeded.

251
00:13:25,084 --> 00:13:26,095
And let's print

252
00:13:29,084 --> 00:13:30,075
and yes,

253
00:13:30,076 --> 00:13:36,068
we see now that the CBFS file tool compressed is compressed

254
00:13:36,069 --> 00:13:38,096
as indicated by the last column,

255
00:13:39,034 --> 00:13:41,069
And it shows that the compressed size as well.

256
00:13:42,024 --> 00:13:46,055
So ??? after compression that is a

257
00:13:46,055 --> 00:13:48,076
bit more than 400 kilos,

258
00:13:50,014 --> 00:13:54,047
and the real find sources over 1.5

259
00:13:54,047 --> 00:13:55,062
of mega bytes.

260
00:13:55,063 --> 00:13:58,026
So, let's see if it is really true.

261
00:14:02,004 --> 00:14:02,041
Yes,

262
00:14:02,042 --> 00:14:03,049
it's actually true.

263
00:14:03,069 --> 00:14:08,069
The file size is the same, so it works.

264
00:14:08,007 --> 00:14:14,002
So we already know how to remove and add

265
00:14:14,002 --> 00:14:14,009
new files,

266
00:14:14,091 --> 00:14:15,099
extract them,

267
00:14:16,000 --> 00:14:17,027
print its contents.

268
00:14:17,094 --> 00:14:21,057
You may also play with the CBFS tool

269
00:14:21,059 --> 00:14:28,097
more for your own exercise. I

270
00:14:28,097 --> 00:14:30,072
recommend at some integers,

271
00:14:30,073 --> 00:14:33,025
play with at stages at payloads.

272
00:14:33,084 --> 00:14:39,017
You can also try to bring to save the

273
00:14:39,017 --> 00:14:41,076
layoffs or locates on files, etc.

274
00:14:43,034 --> 00:14:44,006
Good luck!

