# What is coreboot?

.center[.image-30[![](images/coreboot-1024x1024.png)]]

**coreboot** is an extended firmware platform that delivers a lightning fast
and secure boot experience on modern computers and embedded systems. As an Open
Source project it provides auditability and maximum control over technology.

.center[https://coreboot.org]

---

# What is coreboot? #2

.center[.image-14[![](images/ron.jpg)]]

* **coreboot** was created in 1999 by Ron Minnich
* Motivation: "Why I have to press F1 on all this PCs in cluster?"
* support: ARM, ARM64, MIPS (up to 4.11), PowerPC, RISC-V and x86
* 3mdeb started work on supporting POWER9
* please note **coreboot** is always written with small letters
* in favour of single responsibility principle - do one thing and do it good -
  in this case do minimal platform initialization to reliably run further software

```
Ron picture: https://osfc.io
```

???

* difference with UEFI code base is, that coreboot out of the box support 287
  boards from 70 vendors.
* coreboot has different target then U-Boot, it is interested more in servers,
  desktops and recently with embedded systems

http://www.h-online.com/open/features/The-Open-Source-BIOS-is-Ten-An-interview-with-the-coreboot-developers-746525.html

---

# Firmware jargon

* **BIOS** - _Basic Input/Output System_ is a low level software (aka firmware)
responsible for hardware initialization after platform power-on
  - since 2010 industry migrate to UEFI specification based firmware implementation
  - usage of BIOS word is confusing, even hardware OEM/ODM give it many
    meanings "type of firmware/software" and "early hardware initialization
    software"
* **UEFI** - _Unified Extensible Firmware Specification_ is specification
  managed by UEFI Forum, first published in 2007, but its origins are in
  mid-1990s
  - this term is also confused with firmware implementation, but UEFI is just spec
  - reference implementation of UEFI is EDK2 publicly available as source code on Github
* **FSP** - _Firmware Support Package_ is a binary released by Intel for given
  microarchitecture, its goal is to perform initialization critical from
  "intellectual property" point of view

???

* This is not firmware history lecture but we have to briefly explain

---

# Firmware jargon #2

* **AGESA** - _AMD Generic Encapsulated Software Architecture_ for simplicity
  we can say it is like FSP, but for AMD-based platforms
* **binary blob** - probably most frequently used term in Open Source Firmware Community
  - it is official term for software distributed in binary form only
  - term blob came from 1958 sci-fi horror about "growing, corrosive, alien
  amoeboidal entity that crashes to Earth from outer space"
  - FSP and AGESA are called binary blobs
  - OSF community believe binary blobs are damaging to firmware industry,
    because with them firmware is not fully open
* **libreboot** - coreboot "fork" also called deblobed coreboot
  - motivation: "open source fails to promote freedom"

---

# Where is firmware

.center[.image-75[![](images/fw-everywhere.png)]]

---

# OSF ecosystem

.center[.image-90[![](images/osf.svg)]]

---

# coreboot brief history

* **v1** started in 1999 as LinuxBIOS in Los Alamos National Labs by Ron
  Minnich
    - goal was to boot Linux as fast as possible
    - it was achieved by booting Linux from flash
    - this was LinuxBIOS v1 dated 1999-2000
* **v2** 2000-2005
    - x86, Alpha and PowerPC support was added
    - project faced DDR initialization problem, because of rising complexity of
      memory technology
    - ROMCC was created to change C in stackless assembly
    - device tree concept was introduced
    - payloads were introduced for flexible boot support
    - this period had substantial support from silicon vendors:
        - Intel
        - AMD
        - VIA
        - SIS

???

* ROMCC used internal CPU registers to keep variables
* This is needed in early initialization when DRAM is not yet available
* coreboot device tree is different then Linux kernel device tree

---

# coreboot brief history #2

* **v2+** 2005-2008
    - Cache as RAM was introduced
    - AMD64 port
    - ACPI and SMM support implementation
    - flashrom
    - FSF ported coreboot and started use on their servers
* **v3** 2006-2008
    - 100+ mainboards supported
    - v3 was experiment to fix major problems
    - predecessor of CBFS was created called LAR (LinuxBIOS ARchive)
    - v3 was abandoned and all major improvements were backported to v2
* 2008 LinuxBIOS was renamed to coreboot
* **v4** 2009-2012
    - move from svn to git
    - big contribution from AMD - AGESA (AMD Generic Encapsulated Software
      Architecture)

???

* when coreboot was created Stefan took over maintainership

---

# coreboot brief history #3

* **v4+** 2012-
    - Google introduced first Chromebook with coreboot
    - Intel provide Firmware Support Package (FSP)

.center[.image-65[![](images/coreboot_releases.png)]]

???

* at that point I worked for Intel - it was blasting information for me
* lot of good recently is related to
    - WikiLeaks vault7 publications
    - Snowden revelations
    - major security failures from big silicon vendors and IBVs
* 4.12 was released 12. May 2020

---

# Industry commitment

.right-column50[
.center[.image-99[![](images/tesla.png)]]
]
.left-column50[
.center[.image-99[![](images/ocp.png)]]
]
.center[.image-60[![](images/supermicro.png)]]
* Also: Google, Siemens, AMD, Intel, Mediatek, and many more.
* Why?
  * it's free (as in free beer)
  * it's very simple design, does minimum to boot the OS
  * it's vendor independent, cross-platform and fast

???

Code quality is much better especially vs BIOS written in
assembly language. It is very important because those company
want to customize firmware in many ways that is not supported
by BIOS/UEFI. This includes performance and power saving
tweaks, adding support for new hardware and disabling features
(eg, Siemens pointed out that it needed to change UEFI code
to disable setup screen – link end the end of comment).

Being cross-platform is not only possibility to run it on
many platform. In consequence, it can be known better.

In this source it's pointed out that UEFI builds only on
Windows: https://www.coreboot.org/images/5/50/An_Open_Source_EC.pdf

https://ecc2017.coreboot.org/uploads/talk/presentation/36/SINUMERIK___step_ahead_with_coreboot.pdf

---

# Industry commitment #2

.left-column50[
.center[.image-80[![](images/system76.png)] .image-80[![](images/purism.png)]]
]

.right-column50[
.center[.image-99[![](images/protectli.png)] .image-99[![](images/insurgo.png)]]
]

---

# Government organization commitment

.center[.image-20[![](images/bsi.png)]]

* Federal Office for Information Security (BSI) Germany is
an example of state agency that value coreboot also because
it is easier to audit and it can be freely forked and changed
if needed.

.center[.image-60[![](images/nsa.png)]]

* Thanks to open-source, good coding practices and simple design,
coreboot is considered more secure and much easier to reason about than
proprietary UEFI and BIOS implementations.

---

# Development environment preparation

* Linux is preferred development environment for Open Source Firmware
  (including coreboot)
  - any modern Linux distribution that support Docker should work
* What is Docker and why we use it?
  - Docker is a container technology
  - container is OS-level virtualization technology that allows existence of
    multiple isolated user space instances
  - embedded software/firmware requires little bit different building
    environment, configuring those development environments on your host may
    quickly make a mess in your system
  - when we perform training we don’t want to waste time for users to
    reconfigure or clean the environment, containers help achive that goal
* To install Docker in your Linux distribution please follow official Docker
  installation guide: https://docs.docker.com/engine/install

.footnote[https://containers.3mdeb.com/]
.footnote[https://blog.3mdeb.com/2018/2018-09-27-optimize-performance-in-docker/]

---

# Git guidelines

* First, get complete source code using git and install git
hooks:
```shell
git clone --recurse-submodules https://review.coreboot.org/coreboot.git
cd coreboot
make gitconfig
```

* Those hooks provide that basic lint will be done before
commit and Change-Id will be appended to commit message
(gerrit requires that).
* All commits must be signed-off with real name. This means
that you claim what Developer's Certificate of Origin 1.1
says
```shell
git commit -s
```

Does it automatically if you have real name set in git-config

---

# Practice #1: Clone coreboot and setup git

???

coreboot Git info: https://www.coreboot.org/Git
Signed-off procedure: https://www.coreboot.org/Development_Guidelines#Sign-off_Procedure

Practice:

To avoid potential problems that my existing configuration may cause I decided
to show you some steps in clean Debian containers. You can exercise using
container of your choice and them move configuration to your host. Please note
leaving container means loosing all changes except those in workspace directory.

```
mkdir workspace
docker run -v $PWD/workspace:/workspace -w /workspace --rm -it debian:stable /bin/bash
(docker)# apt update
(docker)# apt install git build-essential vim
(docker)# git config --global user.name "Joe Doe"
(docker)# git config --global user.email joe.doe@example.com
(docker)# cat ~/.gitconfig
(docker)# git clone --recurse-submodules https://review.coreboot.org/coreboot.git
(docker)# cd coreboot
(docker)# make gitconfig
```

Modify README.md

````
(docker)# git commit -s
```

---

# Useful git tricks

* We recommend mastering git command line, if you plan to work with open source
projects like coreboot
  - UI tools hide too much and may cause hard to solve problems
* Useful aliases from my `~/.gitconfig`
```shell
[alias]
  ci = commit -s
  co = checkout
  br = branch
  st = status
  df = diff
  dc = diff --cached
  lol = log --graph --decorate --pretty=oneline --abbrev-commit
  lola = log --graph --decorate --pretty=oneline --abbrev-commit --all
  lolb = log --graph --decorate --pretty=format:'%Cred%h%Creset
-%C(yellow)%d%Creset %s %Cgreen(%cr) %C(bold blue)<%an>%Creset' --abbrev-commit
--all --simplify-by-decoration
  hist = log --pretty=format:\"%h %ad|%s%d [%an]\" --graph --date=short
```

???

https://github.com/pietrushnic/dotfiles/blob/master/gitconfig

---

# Practice #2: Try various git aliases

???


---

# Toolchain

* coreboot use very specific toolchain (exact versions
of software are required).
  - coreboot project want to independent
  - distro provided toolchains are broken and full of assumptions
  - coreboot project should build on various OSes and distros (BSD, CygWin)
  - coreboot project supports multiple CPU architecture
* Standard procedure for automatic toolchain build is:
```shell
make crossgcc       #make toolchain for all architectures
make crossgcc-i386  #make x86 toolchain
make crossgcc-x64   #make x86_64 toolchain
```
* This procedure is time-consuming and may fail. That's
why we recommend using docker image with pre-built toolchain.
* Basic way of checking if Docker is correctly installed is calling as user:
```shell
docker info
```

---

# coreboot-sdk

* Other useful thing is to check what images you have installed
```shell
docker images|grep coreboot
```

* **coreboot/coreboot-sdk** is official coreboot release.
For this training we use extended version:
**3mdeb/coreboot-training-sdk**:
```shell
docker pull 3mdeb/coreboot-training-sdk:4.12.1
```

* Versioning scheme is as follows:
```shell
<coreboot_major_ver>.<coreboot_minor_ver>.<coreboot-training-sdk_patch_number>
```

* You may check available versions on Docker Hub: https://hub.docker.com/r/3mdeb/coreboot-training-sdk
* Official coreboot-sdk: https://hub.docker.com/r/coreboot/coreboot-sdk

---

# Practice #3: Pull Dockerized SDKs

---

# coreboot-training-sdk

* Key difference between official and 3mdeb SDK:
  - bugs fixed
  - tools and scripts, which simplify coreboot training program
* To start shell in docker environment (assuming you are in
coreboot source directory):
```shell
docker run -w /home/coreboot/coreboot/ -u root -it -v $PWD:/home/coreboot/coreboot --rm  \
    3mdeb/coreboot-training-sdk:4.12.1 /bin/bash
```
 - `docker run` – open new container based on image
 - `-it` – interactive via pseudotty
 - `--rm` – automatic cleanup changes in container
 - `-w` - set start working directory
 - `-v` - mounts volume inside container
* If `3mdeb/coreboot-training-sdk` image is not available it would be pulled

???

Docker container may be built from source as well:
\
Or built from source:

```
git clone https://github.com/3mdeb/coreboot.git -b coreboot_training_4.12.1
cd coreboot/util/docker
DOCKER_COMMIT=4.12 COREBOOT_IMAGE_TAG=4.12.1 COREBOOT_CROSSGCC_PARAM=build-x64 make coreboot-sdk
```
---

# Docker issues

* Build reproducibility problems
  - Binaries produced by Docker are not fully binary reproducible
* Docker labels may change
* Package versions may change e.g. Debian, Alpine etc.
  - please note frequent `apt-get update && apt-get upgrade -y` in Dockerfiles
  - pinning packages could be solution, but means distro archive old package versions
* No control over packages
  - some licenses may be problematic in embedded environment
  - providing customized packages is very hard
* In ideal world we would use Yocto to build Docker images
  - definitely achievable, but expensive in terms of complexity and resources
* This is ok for training purposes, but be caution when applying Docker-based
  build environment in security/trusted applications

---

# Quiz #1

---

# Demo #1

???

coreboot development environment from scratch

For the purpose of this exercise I will use VM to not interfere with my
host configuration.

1. What I have here is plain Debian 10 osboxes Virtualbox image
2. Four things I did:
- portforwarding in Virtualbox to expose SSH port to host
- network configuration set to dhcp in /etc/network/interfaces in guest
- ssh-copy-id to login without password
- setup apt for Polish server
3. Let's go through setup from scratch
```
apt update
apt install sudo git vim apt-transport-https ca-certificates curl gnupg-agent software-properties-common
```

```
/usr/sbin/usermod -aG sudo osboxes
```

Relogin

```
curl -fsSL https://download.docker.com/linux/debian/gpg | sudo apt-key add -
sudo add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/debian \
   $(lsb_release -cs) \
   stable"
sudo apt update
sudo apt install docker-ce docker-ce-cli containerd.io
```

Add to docker group

```
sudo usermod -aG docker $USER
```

Relogin

```
docker run hello-world
```

Let's change location of our images: https://stackoverflow.com/questions/24309526/how-to-change-the-docker-image-installation-directory/34731550#34731550

```
git clone --recurse-submodules https://review.coreboot.org/coreboot.git
cd coreboot
docker run -w /home/coreboot/coreboot/ -u root -it -v $PWD:/home/coreboot/coreboot --rm  \
      3mdeb/coreboot-training-sdk:4.12.1 /bin/bash
```


---

# Assignment #1

---

# coreboot contribution overview

.center[<img src="images/gerrit.png" width="900px" style="margin-left:-85px;margin-right:-85px"/>]

* patches are managed through Gerrit at https://review.coreboot.org

---

# coreboot contribution overview #2

* Most basic contribution workflow
  - create Gerrit account
  - clone repository
  - checkout branch
  - modify code and test
  - commit and clean according to guidelines
  - push upstream

---

# Development rules

* In most cases you should always branch from coreboot master branch to make
  your changes.
* Code style follows Linux kernel coding style
  - if you don't know it you should read it since many open source projects use it
* Most important rules:
    * K&R code style,
    * use fixed size variable types like: `uint32_t` or `u32`,
    * if you paste code, check if its licence allow that,
    * separating assembly code (avoid inlining), use AT&T syntax,
    * 96 character line width,
    * functions must have prototypes,
    * local functions must be static,
    * no function definitions in headers,
    * and much more :-)

???

* It's good idea to make sure that jenkins won't reject your
change because of code style:
```shell
indent -npro -kr -i8 -ts8 -sob -l96 -ss -ncs <changed_files>
make what-jenkins-does CPUS=4
```

Linux Kernel coding style:
http://git.kernel.org/cgit/linux/kernel/git/torvalds/linux.git/plain/Documentation/CodingStyle?id=HEAD

Full list of DOs and DONTs: https://www.coreboot.org/Development_Guidelines
and: https://www.coreboot.org/Coding_Style

---

# Developer documentation

* Keep documented what you work:
  * design and concepts
  * assumptions, stipulations.
* Document any bugs you find.
* Quality over quantity, keep concise.
* Focus on documentation target: developer, users, vendors, etc.
* Keep style and look consistent with the rest.
* Use https://doc.coreboot.org it delivers robust and growing documentation set
  - wiki at this point is considered deprecated, but still contain valuable information

---

# Committing

* While committing to coreboot repository you should follow Linux Kernel
  submitting patches document
* Most important rules:
  * There should be only one logical change per commit.
  * All commits should contain 3 section (separated with empty lines)
    * 65 characters commit summary
    * any number of lines with explanation of the problem and solution
    * Signed-off-by
* Example:

```
examplecomponent: Refactor duplicated setup into a function

Setting up the demo device correctly requires the exact same register
values to be written into each of the PCI device functions. Moving the
writes into a function allows also otherexamplecomponent to use them.

Signed-off-by: Joe Hacker <joe@hacker.email>
```

???

https://www.kernel.org/doc/html/latest/process/submitting-patches.html#submittingpatches

---

# Before you push

* If needed, rebase to current origin/master and test again.
  - Perform that check after any, even minor, correction
* Make sure that every source file has proper licence notice.
* Make sure that all changes have been committed.
* Gerrit requires that you push to `HEAD:refs/for/master` instead of master or
  dedicated branch.
* Check your changes against basic tests
```
make what-jenkins-does      - Run platform build tests with junit output
make lint / lint-stable     - run coreboot lint tools (all / minimal)
make test-basic             - Run stardard build tests. All expected to pass.
make test-lint              - basic: Run stable and extended lint tests.
```

* If checks pass you can push changes
```shell
git push origin HEAD:refs/for/master
```

---

# Code review

* Review contains comment on changes and a grade meaning:
    * `-2` – definitely don't merge. Reserved only for core
    developers (gerrit won't merge it).
    * `-1` – I prefer you won't merge it
    * `0` – neutral
    * `+1` – looks good, but someone else should check it
    * `+2` – merge it. Never give `+2` to your own change. If
    you give `+2,` you take responsibility for it. If it breaks
    something, you are expected to be involve in fixing it.
* `+2` lets Gerrit merge, but change won't be merged if it
wasn't in review at least 24 hours, so that all developers
from all around the world were able to review.
* Don't expect that anyone will review your change if you don't
ask to. You may assign someone in Gerrit or ask on IRC
channel. You may check in MAINTAINERS file who's in charge.

???

if you give -2, you have to give guidelines how to fix it.

https://review.coreboot.org/cgit/coreboot.git/plain/Documentation/gerrit_guidelines.md

---

# Community

* Documentation: https://doc.coreboot.org
* Wiki: https://www.coreboot.org/Welcome_to_coreboot
* News: https://blogs.coreboot.org
* IRC: #coreboot and #flashrom at chat.freenode.net
* Mailing list: https://mail.coreboot.org/roundcube/
* Gerrit: https://review.coreboot.org
* Source: https://github.com/coreboot/coreboot
* Facebook: https://www.facebook.com/pg/coreboot/community/
* Twitter: https://twitter.com/coreboot_org

IRC page on wiki says that it's used mainly by CET timezone
users.

---

# Quiz #2

** What can you tell about documentation rules? **

--

* Quality over amount
* Use wiki and/or doxygen
* Mind your target


--

** What are committing rules? **

--

* One commit per logical change
* max 65 character summary with changed module name
* more detailed information after blank line
* Signed-off-by and Change-Id after another blank line
(generated automatically).

---

** Who do you ask for review when after pushing your changes? **

--

* person who committed last change on file(s)
* person who made most changes on file(s)
* person listed in MAINTAINERS file

---

# Demo #2

???

Let's setup coreboot project contribution environment configuration

1. Let's login in to https://review.coreboot.org
2. Let's generate ssh keys
```
ssh-keygen -f ~/.ssh/coreboot-training 
```
3. Change origin of coreboot repo to ssh point
4. `make gitconfig`


---

# Assignment #2

---

class: center, middle

# Q&A

## Thank you
