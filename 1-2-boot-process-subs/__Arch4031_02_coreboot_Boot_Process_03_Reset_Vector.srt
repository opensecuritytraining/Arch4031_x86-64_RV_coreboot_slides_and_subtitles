﻿1
00:00:01,280 --> 00:00:06,480
before we start explaining the

2
00:00:03,840 --> 00:00:08,800
boot stages, we need to first know what

3
00:00:06,480 --> 00:00:10,320
happens exactly

4
00:00:08,800 --> 00:00:12,160
when we press the power button of the

5
00:00:10,320 --> 00:00:14,160
machine.

6
00:00:12,160 --> 00:00:15,440
So, the first instruction exited by

7
00:00:14,160 --> 00:00:17,600
processor

8
00:00:15,440 --> 00:00:18,800
when it comes out of reset is reset

9
00:00:17,600 --> 00:00:20,800
vector,

10
00:00:18,800 --> 00:00:22,400
it is like a beginning of the wolrd

11
00:00:20,800 --> 00:00:25,519
universe and everything

12
00:00:22,400 --> 00:00:27,519
but on the silicon level typically it is

13
00:00:25,519 --> 00:00:30,080
located at a fixed memory address

14
00:00:27,519 --> 00:00:30,880
dependent on the architecture for

15
00:00:30,080 --> 00:00:34,160
example on

16
00:00:30,880 --> 00:00:34,640
arm it is a zero address the primary

17
00:00:34,160 --> 00:00:36,719
core

18
00:00:34,640 --> 00:00:37,920
loads the program counter and starts

19
00:00:36,719 --> 00:00:41,920
executing from

20
00:00:37,920 --> 00:00:43,680
address pointed by a core stack primary

21
00:00:41,920 --> 00:00:46,320
core stack pointer

22
00:00:43,680 --> 00:00:47,520
which instructs the core to load its

23
00:00:46,320 --> 00:00:49,840
reset handler

24
00:00:47,520 --> 00:00:52,480
along with stack pointer vector table

25
00:00:49,840 --> 00:00:54,399
and read the processor start address

26
00:00:52,480 --> 00:00:56,480
to get the application boot address and

27
00:00:54,399 --> 00:00:59,520
jump to that location.

28
00:00:56,480 --> 00:01:00,559
On the other hand, on x86 the recent

29
00:00:59,520 --> 00:01:05,920
vector

30
00:01:00,559 --> 00:01:07,920
is located at 16 bytes below 4 gigabytes

31
00:01:05,920 --> 00:01:09,680
but this is what it looked like for most

32
00:01:07,920 --> 00:01:12,400
of the lighting of x86

33
00:01:09,680 --> 00:01:13,680
architecture nowadays the silicon

34
00:01:12,400 --> 00:01:15,680
vendors tend to

35
00:01:13,680 --> 00:01:17,680
break this architectural rule by

36
00:01:15,680 --> 00:01:20,000
changing the reset vector address or

37
00:01:17,680 --> 00:01:22,240
execute the code before the CPUs are

38
00:01:20,000 --> 00:01:24,840
released from the reset

39
00:01:22,240 --> 00:01:26,320
few examples of such behavior are for

40
00:01:24,840 --> 00:01:28,799
example

41
00:01:26,320 --> 00:01:31,200
Intel authenticated code modules

42
00:01:28,799 --> 00:01:34,000
microcode updates

43
00:01:31,200 --> 00:01:36,479
management engine bring up custom resetted

44
00:01:34,000 --> 00:01:39,040
vector in boot guard.

45
00:01:36,479 --> 00:01:40,400
On the other hand AMD also have similar

46
00:01:39,040 --> 00:01:44,240
mechanism like

47
00:01:40,400 --> 00:01:46,399
the PSP bring up and memory training

48
00:01:44,240 --> 00:01:48,399
PSP also copies the firmware to run the

49
00:01:46,399 --> 00:01:49,280
DRAM before the cpus are released from

50
00:01:48,399 --> 00:01:51,600
reset

51
00:01:49,280 --> 00:01:55,840
or can also have some custom entry point

52
00:01:51,600 --> 00:01:55,840
for the first instructions.

