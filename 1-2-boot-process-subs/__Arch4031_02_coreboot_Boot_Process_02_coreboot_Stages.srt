﻿1
00:00:00,000 --> 00:00:02,350
So, I have mentioned the coreboot stages,

2
00:00:02,950 --> 00:00:05,100
but I haven't yet explained what these are

3
00:00:05,100 --> 00:00:07,110
and how they are named.

4
00:00:07,700 --> 00:00:08,850
So basically,

5
00:00:08,850 --> 00:00:10,650
coreboot consists of

6
00:00:11,850 --> 00:00:14,350
5 to 6 main stages,

7
00:00:14,850 --> 00:00:16,600
and the names are as follows:

8
00:00:16,600 --> 00:00:17,900
The bootblock,

9
00:00:17,900 --> 00:00:21,050
it is the earliest stage that is executed,

10
00:00:21,050 --> 00:00:26,020
right after waking up the machine from sleep.

11
00:00:27,000 --> 00:00:29,700
Then there is an optional verstage,

12
00:00:29,700 --> 00:00:33,720
verstage is responsible for early firmware verification

13
00:00:34,510 --> 00:00:37,910
and is tied to the verified boot implementation

14
00:00:38,800 --> 00:00:40,930
Then we have romstage,

15
00:00:40,940 --> 00:00:45,310
which main task is to prepare the main memory.

16
00:00:46,750 --> 00:00:49,050
Then we have postcar,

17
00:00:49,050 --> 00:00:53,150
which tears down the temporary memory environment

18
00:00:53,150 --> 00:00:54,400
and prepares

19
00:00:55,150 --> 00:00:57,050
the next stage to be run

20
00:00:57,050 --> 00:01:00,650
from the main RAM memory.

21
00:01:01,000 --> 00:01:04,400
And then we have the last coreboot stage,

22
00:01:04,400 --> 00:01:06,110
which is called ramstage

23
00:01:06,400 --> 00:01:10,200
And it is responsible for overall hardware initialization,

24
00:01:11,100 --> 00:01:15,850
writing various tables that are passed to the operating system,

25
00:01:15,850 --> 00:01:17,902
and handover to the payload.

26
00:01:18,450 --> 00:01:20,990
Payload is not really a part of coreboot,

27
00:01:21,600 --> 00:01:25,350
but it is the last stage that

28
00:01:25,350 --> 00:01:28,150
coreboot hands over the control to.

29
00:01:28,150 --> 00:01:30,750
The payload is anything we want it to be,

30
00:01:30,750 --> 00:01:34,450
it can be a bootloader on an application

31
00:01:34,460 --> 00:01:37,400
It may load target operating system, etc.

32
00:01:38,600 --> 00:01:42,380
So, each of these stages has its own purpose and

33
00:01:42,390 --> 00:01:45,510
one should not perform tasks of one stage in another.

34
00:01:46,200 --> 00:01:49,010
It is enforced by coreboot booting model,

35
00:01:49,010 --> 00:01:52,920
and to keep behavioral consistency across various platforms.

36
00:01:53,550 --> 00:01:57,510
I have mentioned that coreboot consists of multiple stages.

37
00:01:58,000 --> 00:02:00,400
But how does it really work?

38
00:02:00,400 --> 00:02:01,320
Typically,

39
00:02:01,750 --> 00:02:03,800
firmware is a single binary, right?

40
00:02:04,500 --> 00:02:08,300
So, each of the stages are indeed in separate executable file

41
00:02:08,700 --> 00:02:11,800
but they are packed into a CBFS.

42
00:02:12,600 --> 00:02:15,350
CBFS is a coreboot file system

43
00:02:15,350 --> 00:02:17,850
that allows to pack the firmware components

44
00:02:17,850 --> 00:02:20,600
to create a sort of archive.

45
00:02:21,500 --> 00:02:25,100
Of course, there is a tool that allows manipulating the CBFS contents

46
00:02:25,100 --> 00:02:26,520
called cbfstool.

47
00:02:26,900 --> 00:02:29,220
It is a part of the coreboot repository.

48
00:02:30,350 --> 00:02:33,600
It is able to put and remove files from CBFS,

49
00:02:34,400 --> 00:02:36,300
apply compression to the files

50
00:02:36,300 --> 00:02:38,750
and force their alignment or base address

51
00:02:38,750 --> 00:02:40,350
in the main memory.

52
00:02:40,700 --> 00:02:44,820
It is also capable of manipulating the flashmap partition contents.

53
00:02:45,200 --> 00:02:45,910
What is flashmap?

54
00:02:46,110 --> 00:02:47,510
I will describe soon.

55
00:02:48,700 --> 00:02:50,400
Besides these features,

56
00:02:50,400 --> 00:02:52,600
it can also integrate various file types

57
00:02:52,600 --> 00:02:54,550
like bootsplash images,

58
00:02:54,550 --> 00:02:55,850
raw files,

59
00:02:55,850 --> 00:02:57,750
64-bit integers,

60
00:02:57,750 --> 00:02:58,610
etc.

