﻿1
00:00:00,034 --> 00:00:05,046
Other very important feature of QEMU is QEMU monitor,

2
00:00:05,084 --> 00:00:09,008
it is very powerful piece of QEMU.

3
00:00:09,008 --> 00:00:13,015
It gives direct control over the emulated hardware.

4
00:00:13,016 --> 00:00:17,077
We can like art processors and memory to be

5
00:00:17,077 --> 00:00:19,029
honest this QEMU

6
00:00:19,003 --> 00:00:24,027
monitor has all the features that any a professional

7
00:00:24,027 --> 00:00:26,025
cloud provider would need

8
00:00:26,064 --> 00:00:31,014
and even more, so it's very very sophisticated

9
00:00:31,014 --> 00:00:34,041
It gives ability to look into memory directly.

10
00:00:34,042 --> 00:00:37,097
But, in this training we will not

11
00:00:37,097 --> 00:00:40,034
explore it very deeply.

12
00:00:40,034 --> 00:00:43,038
It is very easy to find documentation about all

13
00:00:43,038 --> 00:00:44,016
the features,

14
00:00:44,054 --> 00:00:51,004
let's just show like typically what firware developers use,

15
00:00:51,005 --> 00:00:55,019
they typically check-for example if QEMU

16
00:00:55,002 --> 00:00:59,009
with coreboot hanging at some point we check what

17
00:00:59,009 --> 00:00:59,097
kind of registers,

18
00:00:59,097 --> 00:01:01,041
what's the state of the registers,

19
00:01:01,041 --> 00:01:02,049
where is the stock pointer,

20
00:01:02,049 --> 00:01:06,015
where is the instruction pointer.

21
00:01:06,024 --> 00:01:09,005
So, I will show you how to dump registers,

22
00:01:09,051 --> 00:01:13,086
there are some additional features like enabling GDP server

23
00:01:13,086 --> 00:01:17,000
to hook into QEMU.

24
00:01:17,001 --> 00:01:19,065
But, this will be discussed further

25
00:01:19,066 --> 00:01:23,096
but at this point let's let's discuss how to

26
00:01:24,014 --> 00:01:26,045
connect QEMU monitor.

27
00:01:26,074 --> 00:01:29,001
So, to enable cumin monitor,

28
00:01:29,011 --> 00:01:32,075
we can use various different methods.

29
00:01:32,075 --> 00:01:34,059
We can use standard IO,

30
00:01:34,059 --> 00:01:34,082


31
00:01:34,082 --> 00:01:39,037
we can use like multiple different methods. But, in our

32
00:01:39,037 --> 00:01:41,049
case we will use unique sockets

33
00:01:41,005 --> 00:01:45,034
and then we'll use socket to connect to that

34
00:01:45,034 --> 00:01:46,028
unique socket

35
00:01:46,029 --> 00:01:51,015
and have some fun using QEMU monitor.

36
00:01:52,004 --> 00:01:53,008
So,

37
00:01:53,081 --> 00:01:56,065
the process is not very trivial.

38
00:01:56,066 --> 00:02:01,085
We're running QEMU more with monitor parameters defining where is

39
00:02:01,085 --> 00:02:03,035
the socket location.

40
00:02:04,044 --> 00:02:06,086
Some parameters for socket,

41
00:02:07,054 --> 00:02:10,098
and in that way we

42
00:02:10,098 --> 00:02:18,025
already use default output of QEMU which is saturated

43
00:02:18,025 --> 00:02:20,046
with carbooot locks.

44
00:02:21,004 --> 00:02:23,086
We somehow have to hook into the system.

45
00:02:24,034 --> 00:02:27,082
That's why we just opening another terminal,

46
00:02:27,083 --> 00:02:29,096
we're looking for our container

47
00:02:29,097 --> 00:02:33,051
because QEMU is already in container.

48
00:02:33,052 --> 00:02:38,088
We have to enter the container and find

49
00:02:38,088 --> 00:02:40,071
the socket that connect to the socket,

50
00:02:40,072 --> 00:02:45,017
then we can explore QEMU monitor commands.

51
00:02:45,039 --> 00:02:46,066
So, let's try that.

52
00:02:48,024 --> 00:02:50,076
So, let just execute the monitor.

53
00:02:51,034 --> 00:02:52,079
Of course,

54
00:02:52,008 --> 00:02:56,086
it was shot on the slide which is adding to

55
00:02:56,086 --> 00:02:58,055
the alisas monitor,

56
00:02:59,004 --> 00:03:04,099
unique with the path to the unique socket.

57
00:03:05,028 --> 00:03:10,019
Of course, this would be server and and some parameters

58
00:03:10,019 --> 00:03:11,025
for the socket.

59
00:03:11,094 --> 00:03:17,056
So, we're running let's switch to second terminal.

60
00:03:18,044 --> 00:03:23,009
We need asciinema here also.

61
00:03:23,001 --> 00:03:27,006
And what we have to do here is, identify the

62
00:03:27,006 --> 00:03:30,086
docker container in which we run this QEMU

63
00:03:30,086 --> 00:03:31,066
session.

64
00:03:32,014 --> 00:03:36,067
So, first let's ??? what kind of

65
00:03:36,067 --> 00:03:41,086
Docter I'm running. This is the ID of the container.

66
00:03:42,074 --> 00:03:50,095
So, let's execute some interactive command in this container.

67
00:03:51,074 --> 00:03:55,051


68
00:03:55,051 --> 00:03:56,076
We are inside,

69
00:03:57,014 --> 00:04:01,051
so now we should be able to use socket

70
00:04:01,052 --> 00:04:10,015
command for hooking into the a socket exposed by the

71
00:04:10,016 --> 00:04:10,086
QEMU.

72
00:04:13,022 --> 00:04:14,019
As you can see,

73
00:04:14,019 --> 00:04:15,096
we are in QEMU monitor,

74
00:04:16,054 --> 00:04:20,035
there are there are multiple commands here.

75
00:04:20,094 --> 00:04:25,016
You can see how reach this environment is,

76
00:04:25,074 --> 00:04:28,095
I even don't know all the possibilities here,

77
00:04:29,034 --> 00:04:37,039
but what's important for basic exploration of the possibilities

78
00:04:37,004 --> 00:04:41,075
of QEMU monitor is, info command.

79
00:04:42,024 --> 00:04:45,093
Info command is very important and show quite a lot

80
00:04:45,093 --> 00:04:47,066
of information about the system.

81
00:04:48,034 --> 00:04:49,091
First of all,

82
00:04:49,091 --> 00:04:53,027
it can show information about the processor

83
00:04:53,028 --> 00:04:56,032
it can do the dumps,

84
00:04:56,033 --> 00:05:02,017
it can perform some interesting reports about

85
00:05:02,018 --> 00:05:06,066
intribs a peaks about virtualization,

86
00:05:06,066 --> 00:05:07,075
about memory.

87
00:05:08,024 --> 00:05:11,046
But, what I want to show you here is for

88
00:05:11,046 --> 00:05:14,036
example registers,

89
00:05:17,044 --> 00:05:18,086
and as you can see

90
00:05:19,002 --> 00:05:20,004
we're probably

91
00:05:20,041 --> 00:05:25,042
in a 16 bit mod.

92
00:05:25,043 --> 00:05:34,026
Where our SeaBIOS trying to handle some

93
00:05:34,027 --> 00:05:35,025
disks.

94
00:05:36,024 --> 00:05:39,078
But, you can see that it also change during

95
00:05:39,078 --> 00:05:44,036
the execution. So, you can find the stack pointer,

96
00:05:44,036 --> 00:05:47,045
you can find in the instruction pointer and a bunch

97
00:05:47,045 --> 00:05:49,056
of other interesting things.

98
00:05:49,094 --> 00:05:54,047
There's also a TPM information if you have any

99
00:05:54,047 --> 00:05:55,085
TPM hooked,

100
00:05:56,024 --> 00:06:01,086
you can also look at the structure of the

101
00:06:01,087 --> 00:06:08,041
emulated device, emulated system using Q3. Q3 shows

102
00:06:08,042 --> 00:06:12,048
in a kind of 3 level what kind of devices

103
00:06:12,048 --> 00:06:16,006
with which parameters are emulated in this system,

104
00:06:16,044 --> 00:06:17,048
and thanks to that

105
00:06:17,048 --> 00:06:20,024
you can see like where is the MMIO,

106
00:06:20,024 --> 00:06:23,081
this device and bunch of like how the

107
00:06:23,082 --> 00:06:28,002
based address registers are configured and a bunch of

108
00:06:28,021 --> 00:06:32,026
other important information when you're doing development.

109
00:06:32,074 --> 00:06:35,091
Feel free to explore this command even more,

110
00:06:35,092 --> 00:06:40,023
it's very interesting and definitely it is worth to learn

111
00:06:40,023 --> 00:06:41,026
more about it.

