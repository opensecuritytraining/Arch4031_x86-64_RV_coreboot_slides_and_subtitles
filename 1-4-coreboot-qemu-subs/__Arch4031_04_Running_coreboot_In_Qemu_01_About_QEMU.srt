﻿1
00:00:01,044 --> 00:00:02,017
As I said,

2
00:00:02,018 --> 00:00:06,062
QEMU is open source emulator for various platforms.

3
00:00:06,063 --> 00:00:09,039
It supports multiple architectures,

4
00:00:09,004 --> 00:00:12,085
it follows the next philosophy,

5
00:00:12,086 --> 00:00:18,041
it's fully software implementation of various

6
00:00:18,041 --> 00:00:19,026
environments.

7
00:00:19,064 --> 00:00:21,009
What is very important,

8
00:00:21,091 --> 00:00:28,002
it can leverage hardware virtualization features to speed up our

9
00:00:28,002 --> 00:00:33,093
boot process or our emulation process, or this is

10
00:00:33,093 --> 00:00:38,062
like a para virtualization in this case it supports

11
00:00:38,062 --> 00:00:45,019
both KVM as well as HAXM

12
00:00:45,002 --> 00:00:47,086
on Windows,

13
00:00:47,097 --> 00:00:50,039
but in this case this is not so important for

14
00:00:50,039 --> 00:00:50,076
us.

15
00:00:51,024 --> 00:00:54,002
It supports many architectures,

16
00:00:54,021 --> 00:00:55,034
for example,

17
00:00:55,034 --> 00:00:56,021
for Intel,

18
00:00:56,021 --> 00:01:01,009
it supports VTD but what is also important,

19
00:01:01,001 --> 00:01:04,000
it supports risk five and open power.

20
00:01:04,001 --> 00:01:08,000
This helped importing coreboot on top of this

21
00:01:08,001 --> 00:01:11,017
emulation platforms and of course,

22
00:01:11,018 --> 00:01:17,002
bring coreboot project closer to real hardware implementation

23
00:01:17,021 --> 00:01:18,056
of these architectures.

24
00:01:19,064 --> 00:01:20,044
It's it's very,

25
00:01:20,044 --> 00:01:22,011
very subtle tool,

26
00:01:22,012 --> 00:01:22,087
has many,

27
00:01:22,087 --> 00:01:23,065
many options,

28
00:01:23,065 --> 00:01:28,003
like you can probably do training only about QEMU,

29
00:01:28,031 --> 00:01:33,005
it is used widely by cloud providers and various

30
00:01:33,005 --> 00:01:35,066
important vendors.

31
00:01:36,014 --> 00:01:37,076
But from our perspective,

32
00:01:37,077 --> 00:01:42,009
it most important features are for firmware booting,

33
00:01:42,001 --> 00:01:43,002
luckily

34
00:01:43,021 --> 00:01:44,001
QEMU

35
00:01:44,001 --> 00:01:48,034
can give you ability to support whatever firmware

36
00:01:48,034 --> 00:01:49,032
you want to hook,

37
00:01:49,033 --> 00:01:52,004
it also support UEFI EDK2,

38
00:01:52,005 --> 00:01:52,086
about coreboot

39
00:01:52,086 --> 00:01:54,041
is always also supported,

40
00:01:54,042 --> 00:01:55,016
and that's great.

41
00:01:55,034 --> 00:01:58,016
We will use that to learn more about coreboot.

